//
//  Const.swift
//  Krepta
//
//  Created by Beautistar on 6/9/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import Foundation

struct Constant {
    
    static let SAVE_ROOT_PATH = "Krepta"
    
    static let BASE_URL = "http://18.220.26.10/index.php/api/"
    
    static let REQ_CHECKUSERNAME = BASE_URL + "checkUserName/"
    static let REQ_CHECKUSEREXIST = BASE_URL + "checkUserExist/"
    static let REQ_CHECKEMAILEXIST = BASE_URL + "checkEmailExist/"
    static let REQ_UPLOADIMAGE = BASE_URL + "uploadImage/"
    static let REQ_REGISTER = BASE_URL + "register/"
    static let REQ_GETHOMEUSER = BASE_URL + "getUsers/"
    static let REQ_SENDINVITE = BASE_URL + "sendInvite/"
    static let REQ_GETINVITE = BASE_URL + "getReceivedInvite/"
    static let REQ_REPLYINVITE = BASE_URL + "replyInvite/"
    static let REQ_CONFIRMRINVITE = BASE_URL + "confirmInvite/"
    static let REQ_SETRATE = BASE_URL + "setRate/"
    static let REQ_GETRATE = BASE_URL + "getRate/"
    static let REQ_SEARCHUSERS = BASE_URL + "searchUsers/"
    static let REQ_ADDFRIEND = BASE_URL + "addFriend/"
    static let REQ_GETFRIEND = BASE_URL + "getFriends/"
    static let REQ_ACCEPTFRIEND = BASE_URL + "acceptFriend/"
    static let REQ_GETREQUESTEDFRIEND = BASE_URL + "getRequestedFriends/"

    static let REQ_GETPLAY_CHAT_USERS = BASE_URL + "getReadyPlayChatUsers/"
    static let REQ_SENDTOREADYPLAY = BASE_URL + "sendFileToReadyPlay/"
    static let REQ_GETMESSAGES = BASE_URL + "getMessageContent/"
    static let REQ_SENDMESSAGE = BASE_URL + "sendMessage/"
    static let REQ_SENDMESSAGEFILE = BASE_URL + "sendMessageWithFile/"
    static let REQ_REGISTERTOKEN = BASE_URL + "registerToken/"
    static let REQ_UPDATEPROFILE = BASE_URL + "updateProfile/"
    static let REQ_UPDATESTATUS = BASE_URL + "updateStatus/"
    static let REQ_SETREADMSG = BASE_URL + "setReadMessage/"
    
    
    static let RES_RESULTCODE = "result_code"
    static let RES_ID = "id"
    static let RES_PHOTOURLS = "photo_urls"
    static let RES_PHOTOURL = "photo_url"
    static let RES_USERINFO = "user_info"
    static let RES_RECEIVED_INVITE = "received_invite"
    static let RES_PLAYUSER_INVITE = "play_users"
    static let RES_CHATLISTUSER = "message_users"
    static let RES_MESSAGEINFO = "message_infos"
    
    
    static let CODE_SUCCESS = 0

    

    static let EMAIL = "email"
    
    static let AGE = "age"
    static let GENDER = "gender"
    static let LAT = "lat"
    static let LNG = "lng"
    static let PASSWORD = "password"
 
    static let mainColor = UIColor.init(red: 147, green: 168, blue: 233)
    static let chatVCBGColor = UIColor.init(red: 104, green: 117, blue: 133)
    static let grayColor = UIColor.init(red: 245, green: 245, blue: 245)
    static let darkGrayColor = UIColor.init(red: 200, green: 200, blue: 200)
    static let barTintColor = UIColor.init(red: 147, green: 168, blue: 233)
    
    static let FROM_INVITES = 111
    static let FROM_HOME = 222
    static let FROM_CHATLIST = 333
    static let FROM_CHATTING = 444
    static let FROM_MYFRIEND = 555
    static let FROM_RATE = 666
    static let FROM_PROFILE = 777
    static let FROM_READYPLAYLIST = 888
    
    
}
