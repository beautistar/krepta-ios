//
//  CommonUtils.swift
//  Krepta
//
//  Created by Beautistar on 12/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import Foundation

var isReply:Bool = false
var _startAge:Int = 0
var _endAge:Int = 100
var _gender:String = "Anyone"
var _distance:Int = 0

var FBAlbum = [FBAlbumEntity]()
var _selectedAlbumPhotoURL = ""

var _inviteViewed:Bool = false

var _loggedIn = false

func isValidEmail(testStr:String) -> Bool {
    
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: testStr)
    
}

func resizeImage(srcImage: UIImage) -> UIImage {
    
    if (srcImage.size.width >= srcImage.size.height) {
        
        return srcImage.resizedImage(byMagick: "256")
    } else {
        
        return srcImage.resizedImage(byMagick: "x256")
    }
}

// save image to a file (Documents/SmarterApp/temp.png)
func saveToFile(image: UIImage!, filePath: String!, fileName: String) -> String! {
    
    let outputFileName = fileName
    
    let outputImage = resizeImage(srcImage: image)
    
    let fileManager = FileManager.default
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
    var documentDirectory: NSString! = paths[0] as NSString!
    
    // current document directory
    fileManager.changeCurrentDirectoryPath(documentDirectory as String)
    
    do {
        try fileManager.createDirectory(atPath: filePath, withIntermediateDirectories: true, attributes: nil)
    } catch let error as NSError {
        print(error.localizedDescription)
    }
    
    documentDirectory = documentDirectory.appendingPathComponent(filePath) as NSString!
    let savedFilePath = documentDirectory.appendingPathComponent(outputFileName)
    
    // if the file exists already, delete and write, else if create filePath
    if (fileManager.fileExists(atPath: savedFilePath)) {
        do {
            try fileManager.removeItem(atPath: savedFilePath)
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
    } else {
        fileManager.createFile(atPath: savedFilePath, contents: nil, attributes: nil)
    }
    
    if let data = UIImagePNGRepresentation(outputImage) {
        
        do {
            try data.write(to:URL(fileURLWithPath:savedFilePath), options:.atomic)
        } catch {
            print(error)
        }
        
    }
    
    return savedFilePath
}
