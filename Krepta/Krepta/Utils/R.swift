//
//  R.swift
//  Krepta
//
//  Created by Beautistar on 12/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import Foundation

struct R {
    
    struct string {
        
        static let APP_TITLE = "Krepta"
        
        static let OK = "Ok"
        static let CANCEL = "Cancel"
        static let ERROR = "Error Occured!"
        static let CONNECT_FAIL = "Connection to the server failed.\nPlease try again."
        static let UPLOAD_FAIL = "Failed to file send."
        
        
        static let INPUT_USERNAME = "Please input vaild username."
        static let ENABLE_LOCATION = "Please your location service."
        static let EMAIL_EXIST = "Email already exists."        
        
        static let CAMERA_ROLL = "CAMERA ROLL"
        static let FACEBOOK = "FACEBOOK"

        static let FILE_TYPE = "File types"
        static let FILE_IMAGE = "Image"
        static let FILE_VIDEO = "Video"

    }
}
