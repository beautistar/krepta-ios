//
//  FBAlbumEntity.swift
//  Krepta
//
//  Created by Beautistar on 8/14/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import Foundation

class FBAlbumEntity: NSObject, NSCoding {
    
    var _id = ""
    var _name = ""
    var _pictureUrls = [String]()
    
    override init() {
        
    }
    
    required init(coder decoder: NSCoder) {
        self._id = decoder.decodeObject(forKey: "f_id") as? String ?? ""
        self._name = decoder.decodeObject(forKey: "f_name") as? String ?? ""
        self._pictureUrls = decoder.decodeObject(forKey: "f_pictureUrls") as? [String] ?? []
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(_id, forKey: "f_id")
        coder.encode(_name, forKey: "f_name")
        coder.encode(_pictureUrls, forKey: "f_pictureUrls")
    }
}

