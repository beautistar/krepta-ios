//
//  ChatListUserEntity.swift
//  Krepta
//
//  Created by Beautistar on 7/21/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import Foundation

class ChatListUserEntity {
    
    var _chat_user = UserEntity()
    var _message = ""
    var _time = ""
    var _message_type = ""//received, sent
    var _content_type = 0//0:text, 1:image, 2:video
    var _read_status = 0//0:unread, 1:read
}
