//
//  File.swift
//  Krepta
//
//  Created by Beautistar on 18/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import Foundation

class UserEntity : Equatable {
    
    var _id = 0
    var _username = ""
    var _name = ""
    var _email = ""
    var _age = 0
    var _gender = ""    
    var _profileUrls:[String] = ["", "", "", ""]
    var _latitude = 0.0
    var _longitude = 0.0
    var _distance = 0.0
    var _condition = ""
    var _rate = 0.0
    var _status = ""
    
    static func ==(lhs:UserEntity, rhs:UserEntity) -> Bool {
        return lhs._id == rhs._id
    }
}
