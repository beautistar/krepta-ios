//
//  RateEntity.swift
//  Krepta
//
//  Created by Beautistar on 7/30/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import Foundation

class RateEntity {
    
    var _challenge_id = 0
    var _rate_user = UserEntity()
    var _is_rating = 0
    var _is_rated = 0
    var _rated_score = 0.0
    var _rating_score = 0.0
}

