//
//  InviteEntity.swift
//  Krepta
//
//  Created by Beautistar on 7/17/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import Foundation

class InviteEntity {
    
    var _inviteUser = UserEntity()
    var _invite_condition = ""
    var _reply_condition = ""
    var _request_userid = 0
    var _status = 0
}
