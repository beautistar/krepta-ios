//
//  NewFriendCollectionCell.swift
//  Krepta
//
//  Created by Beautistar on 18/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class NewFriendCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imvUserPhoto: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnReject: UIButton!
    
    
}
