//
//  SendCell.swift
//  Krepta
//
//  Created by Beautistar on 11/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class SendCell: UITableViewCell {

    @IBOutlet weak var imvUserPhoto: UIImageView!
    @IBOutlet weak var lblMessage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imvUserPhoto.layer.borderColor = UIColor.white.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
