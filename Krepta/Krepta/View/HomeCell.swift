//
//  HomeCell.swift
//  Krepta
//
//  Created by Beautistar on 6/10/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import SnapKit

class HomeCell: UITableViewCell {

    @IBOutlet weak var swipeContainer: UIView!
//    @IBOutlet weak var collectionView: UICollectionView!
//    @IBOutlet weak var lblDescription: UILabel!
    
    var delegate: HomeCellDelegate?
    fileprivate var swipeCard: HomeProfileView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//        swipeContainer.delegate = self
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUserInfo(_ userinfo: UserEntity) {
        
        if swipeCard == nil {
            swipeCard = UINib(nibName: "HomeProfileView", bundle: nil)
                            .instantiate(withOwner: nil, options: nil)[0] as? HomeProfileView
            
            self.swipeContainer.addSubview(swipeCard!)
            swipeCard!.snp.makeConstraints({ (maker: ConstraintMaker) in
                maker.left.top.right.bottom.equalToSuperview()
            })
            swipeCard!.delegate = self
        }
        
        // TODO : 
        swipeCard?.imvSmallProfile.image = nil
        swipeCard?.imvSmallProfile.sd_setImage(with: URL(string: userinfo._profileUrls[0]), placeholderImage: #imageLiteral(resourceName: "img_user"))
        swipeCard?.lblUserName.text = userinfo._name
        swipeCard?.lblDistance.text = String(Int(round(userinfo._distance * 0.621371)))
        swipeCard?.lblAge.text = String(userinfo._age)
        swipeCard?.lblDescription.text = userinfo._status
        
        if userinfo._profileUrls.count > 0 {
            swipeCard?.imvBigProfile.image = nil
            swipeCard?.imvBigProfile.sd_setImage(with: URL(string: userinfo._profileUrls[1]), placeholderImage: #imageLiteral(resourceName: "img_user"))
        } else {
            swipeCard?.imvBigProfile.image = #imageLiteral(resourceName: "img_user")
        }

    }

    func swipeRight() -> Void {
        swipeCard?.rightClickAction()
    }
    
    func swipeLeft() -> Void {
        swipeCard?.leftClickAction()
    }
}

extension HomeCell: DraggableViewDelegate {
    
    func cardSwipedLeft(_ card: UIView!) {
        delegate?.homeCellSwiped(self, isToRight: false)
        swipeCard = nil
    }
    
    func cardSwipedRight(_ card: UIView!) {
        delegate?.homeCellSwiped(self, isToRight: true)
        swipeCard = nil
    }
}

protocol HomeCellDelegate {
    
    func homeCellSwiped(_ cell: HomeCell, isToRight: Bool)
}
