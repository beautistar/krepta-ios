//
//  AlbumPhotoCell.swift
//  Krepta
//
//  Created by Beautistar on 8/14/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class AlbumPhotoCell: UICollectionViewCell {
    
    @IBOutlet weak var imvPhoto: UIImageView!
}
