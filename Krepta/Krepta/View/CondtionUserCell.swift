//
//  CondtionUserCell.swift
//  Krepta
//
//  Created by Beautistar on 15/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class CondtionUserCell: UITableViewCell {

    @IBOutlet weak var imvPhoto: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
