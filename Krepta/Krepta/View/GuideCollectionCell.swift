//
//  GuideCollectionCell.swift
//  Krepta
//
//  Created by Beautistar on 27/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class GuideCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imvLeft: UIImageView!
    @IBOutlet weak var imvRight: UIImageView!
    @IBOutlet weak var lblStep: UILabel!
    @IBOutlet weak var lblGuide: UILabel!
}
