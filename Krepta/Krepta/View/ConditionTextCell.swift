//
//  ConditionTextCell.swift
//  Krepta
//
//  Created by Beautistar on 15/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class ConditionTextCell: UITableViewCell {

    @IBOutlet weak var lblCondition: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
