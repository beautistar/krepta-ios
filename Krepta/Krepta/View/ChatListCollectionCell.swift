//
//  ChatListCollectionCell.swift
//  Krepta
//
//  Created by Beautistar on 11/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class ChatListCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imvPhoto: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        
//        imvPhoto.layer.borderColor = UIColor.red.cgColor
//        
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        imvPhoto.layer.borderColor = UIColor.red.cgColor
    }
    
}
