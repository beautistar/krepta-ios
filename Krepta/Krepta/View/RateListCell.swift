//
//  RateListCell.swift
//  Krepta
//
//  Created by Beautistar on 12/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import Cosmos

class RateListCell: UITableViewCell {

    @IBOutlet weak var imvPhoto: UIImageView!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var lblRate: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var lblWaiting: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
