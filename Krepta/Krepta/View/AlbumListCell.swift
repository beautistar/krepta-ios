//
//  AlbumListCell.swift
//  Krepta
//
//  Created by Beautistar on 8/14/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class AlbumListCell: UITableViewCell {
    
    @IBOutlet weak var imvPhoto: UIImageView!
    @IBOutlet weak var lblAlbumName: UILabel!
    @IBOutlet weak var lblPhotoCounts: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
