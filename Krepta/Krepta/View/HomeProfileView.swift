//
//  HomeProfileView.swift
//  Krepta
//
//  Created by Mask on 6/21/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class HomeProfileView: DraggableView {
    
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imvSmallProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblDistance: UILabel!//0.621371
    @IBOutlet weak var imvBigProfile: UIImageView!
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }

}
