//
//  ProfileCollectionCell.swift
//  Krepta
//
//  Created by Beautistar on 11/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class ProfileCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imvProfile: UIImageView!
    @IBOutlet weak var lblNameAge: UILabel!
}
