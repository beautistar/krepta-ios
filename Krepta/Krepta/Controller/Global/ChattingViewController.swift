//
//  ChattingViewController.swift
//  Krepta
//
//  Created by Beautistar on 11/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import MobileCoreServices
import Alamofire

class ChattingViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var tvMessage: UITextView!
    @IBOutlet weak var tblChatting: UITableView!
    let _picker: UIImagePickerController = UIImagePickerController()
    var _imgUrl : String?
    var _videoUrl : String?
    var _chat_user = UserEntity()
    var _from = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self._picker.delegate = self
        _picker.allowsEditing = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        initView()
    }
    
    func initView() {
        
        tblChatting.tableFooterView = UIView()
        tblChatting.estimatedRowHeight = 70
        tblChatting.rowHeight = UITableViewAutomaticDimension
        
        getChatMessages()
    }
    
    func getChatMessages() {
        
    }
    
    
    @IBAction func tabAction(_ sender: Any) {
        
        self.view.endEditing(true)
    }
    
    @IBAction func rocketAction(_ sender: Any) {
        
        
    }
    
    //MARK: - tableView delegate & datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.row == 1 || indexPath.row == 3 ) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiveCell") as! ReceiveCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            cell.lblMessage.text = "Funny thinbg is, when you string all of these phrases together into a sentence, and multiple sentences to form a paragraph"
            
            cell.btnProfilePhoto.addTarget(self, action: #selector(gotoProfile), for: .touchUpInside)
            
            return cell
            
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "SendCell") as! SendCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            cell.lblMessage.text = "Hello, nice to meet you"
            return cell
        }
    }
    
    
    func gotoProfile(_ sender:UIButton) {
        
        //got invite decision
        let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "InviteDecideViewController") as! InviteDecideViewController
        profileVC._from = Constant.FROM_CHATTING
        self.navigationController?.pushViewController(profileVC, animated: true)
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendAction(_ sender: Any) {
        
        tvMessage.text = ""
        self.view.endEditing(true)
    }
    
    @IBAction func attachAction(_ sender: Any) {
        
        doAttach()
    }
    
    
    // MARK: - Attact Image/Video

    func doAttach() {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
        {
            let fileTypeAlert: UIAlertController = UIAlertController(title: R.string.FILE_TYPE, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let imageAction: UIAlertAction = UIAlertAction(title: R.string.FILE_IMAGE, style: UIAlertActionStyle.default, handler: {
                (photoSourceAlert) -> Void in
                self._picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                let kUTTypeImageAnyObject : AnyObject = kUTTypeImage as AnyObject
                self._picker.mediaTypes = [kUTTypeImageAnyObject as! String]
                self.present(self._picker, animated: true, completion: nil)
            })
            
            let videoAction: UIAlertAction = UIAlertAction(title: R.string.FILE_VIDEO, style: UIAlertActionStyle.default, handler: {
                (photoSourceAlert) -> Void in
                
                let kUTTypeMovieAnyObject : AnyObject = kUTTypeMovie as AnyObject
                let kUTTypeMpegAnyObject : AnyObject = kUTTypeMPEG2Video as AnyObject
                let kUTTypeVideoAnyObject : AnyObject = kUTTypeVideo as AnyObject
                self._picker.mediaTypes = [kUTTypeMovieAnyObject as! String, kUTTypeMpegAnyObject as! String, kUTTypeVideoAnyObject as! String]
                self.present(self._picker, animated: true, completion: nil)
            })
            
            fileTypeAlert.addAction(imageAction)
            fileTypeAlert.addAction(videoAction)
            fileTypeAlert.addAction(UIAlertAction(title: R.string.CANCEL, style: UIAlertActionStyle.cancel, handler: nil))
            
            self.present(fileTypeAlert, animated: true, completion: nil);
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            
            //self._imgUrl.image = pickedImage
            _imgUrl = saveToFile(image: pickedImage, filePath: Constant.SAVE_ROOT_PATH, fileName: "image.png")
            print(_imgUrl ?? "NONE")
        
        }
        
        if let videoURL = info[UIImagePickerControllerMediaURL] as? NSURL {
            print(videoURL)
        }
        
        dismiss(animated: true, completion: nil)

    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }
}
