//
//  ChatViewController.swift
//  Krepta
//
//  Created by Beautistar on 7/23/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import Alamofire
import SwiftyJSON
import MobileCoreServices


class ChatViewController: JSQMessagesViewController, UIImagePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate{
    
    // MARK: Properties
    private let imageURLNotSetKey = "NOTSET"
    
    let _picker: UIImagePickerController = UIImagePickerController()
    
    private var _messages: [JSQMessage] = []
    private var photoMessageMap = [String: JSQPhotoMediaItem]()
    
    var _target = UserEntity()
    var _roomName = ""
    var _page = 0
    var _from = 0
    var _imgPath = ""
    var _videoPath = ""
    
    static let TYPE_TEXT = 0
    static let TYPE_IMAGE = 1
    static let TYPE_VIDEO = 2
    
    
    var outgoingBubble: JSQMessagesBubbleImage! // = self.setupOutgoingBubble()
    var incomingBubble: JSQMessagesBubbleImage! // = self.setupIncomingBubble()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        outgoingBubble = self.setupOutgoingBubble()
        incomingBubble = self.setupIncomingBubble()

        
        self._picker.delegate = self
        _picker.allowsEditing = true
        
        self.senderId = String(AppDelegate.getUser()._id)
        self.senderDisplayName = _target._name
        self.title = _target._name
        
        let back = UIBarButtonItem(image:#imageLiteral(resourceName: "ic_arrow_left"), style:.plain, target:self, action:#selector(didTapBack))
        back.tintColor = UIColor.white
        
        let rocket = UIBarButtonItem(image:#imageLiteral(resourceName: "ic_rocket_white"), style:.plain, target:self, action:#selector(didTapRocket))
        rocket.tintColor = UIColor.white
        
        navigationItem.leftBarButtonItem = back
        navigationItem.rightBarButtonItem = rocket
        
        
        self.collectionView.showsVerticalScrollIndicator = false
        self.view.backgroundColor = Constant.chatVCBGColor
        
        let avatarSize = CGSize(width: 40, height: 40)
        
        collectionView!.collectionViewLayout.incomingAvatarViewSize = avatarSize
        collectionView!.collectionViewLayout.outgoingAvatarViewSize = avatarSize
        collectionView!.collectionViewLayout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        collectionView.backgroundColor = UIColor.clear
        
        self.getMessages()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        g_currentVC = self
        
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //self.setRead()
        
    }
    
    func getMessages() {
        
        self._messages.removeAll()
        self.collectionView.reloadData()
        
        self.showLoadingView()
        
        let _url = Constant.REQ_GETMESSAGES + "\(AppDelegate.getUser()._id)/\(self._target._id)"        
        
        print(_url)
        
        Alamofire.request(_url, method:.get)
            .responseJSON { response in
                
                if response.result.isFailure {
                    return
                }
                
                if let result = response.result.value  {
                    
                    let dict = JSON(result)                    
                    
                    let result_code = dict[Constant.RES_RESULTCODE].intValue
                    
                    if result_code == Constant.CODE_SUCCESS {
                        
                        let infos = dict[Constant.RES_MESSAGEINFO].arrayValue
                        
                        for info in infos {
                            
                            
                            var sender = 0
                            var senderName = ""
                            let text = info["content"].stringValue
                            let time = info["reg_date"].stringValue
                            let msg_type = info["message_type"].stringValue
                            
                            
                            if msg_type == "received" {
                                sender = self._target._id
                                senderName = self._target._name
                            } else {
                                
                                sender = AppDelegate.getUser()._id
                                senderName = AppDelegate.getUser()._name
                                
                            }
                            
                            let formatter = DateFormatter()
                            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            formatter.timeZone = TimeZone(identifier:"UTC")
                            let date = formatter.date(from: time)!
                            
                            
                            //msg checking
                            
                            var newMessage : JSQMessage!
                            var newMediaData:JSQMessageMediaData!
                            
                            if info["content_type"].intValue > 0 {
                            
                                switch info["content_type"].intValue {
                                    
                                    case ChatViewController.TYPE_IMAGE :

                                        let photoItemCopy = AsyncPhotoMediaItem(withURL: URL(string: text)!)
                                        
                                            if msg_type == "received" {
                                                photoItemCopy.appliesMediaViewMaskAsOutgoing = false
                                            }
                                        newMediaData = photoItemCopy
                                    
                                    break                                    
                                    
                                    case ChatViewController.TYPE_VIDEO :
                                        
                                        let videoURL = URL(string: text)
                                        let videoItemCopy = JSQVideoMediaItem(fileURL: videoURL, isReadyToPlay: true)
                                    
                                    
                                        if let videoItemCopy = videoItemCopy {
                                            if msg_type == "received" {
                                                videoItemCopy.appliesMediaViewMaskAsOutgoing = false
                                            }
                                        }
                                        
                                        newMediaData = videoItemCopy;
                                    
                                    break
                                    
                                    default :
                                        
                                    assertionFailure("Error: This Media type was not recognised")
                                    }
                                
                                newMessage = JSQMessage(senderId: "\(sender)", senderDisplayName : senderName, date : date, media:newMediaData);
                                
                            } else {
                                
                                newMessage = JSQMessage(senderId: "\(sender)", senderDisplayName: senderName, date : date, text: text.decodeEmoji);
                            }
                            
                            self._messages.append(newMessage)
                            
                        }
                        
                        self.finishReceivingMessage(animated: true)
                        
                        self.hideLoadingView()
                    }
                }
        }
    }
    
    func showMessages(msg:JSQMessage) {
        
    }
    
    func sendMessage(text:String) {
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append("\(AppDelegate.getUser()._id)".data(using:String.Encoding.utf8)!, withName: "user_id")
                multipartFormData.append("\(self._target._id)".data(using:String.Encoding.utf8)!, withName: "sender_id")
                multipartFormData.append(text.encodeEmoji.data(using:String.Encoding.utf8)!, withName: "message")
                
        },
            to: Constant.REQ_SENDMESSAGE,
            encodingCompletion: { encodingResult in
                
        }
        )
    }
    
    func sendMediaMessage(filePath:String, type:Int) {
        
        self.showLoadingView()
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                multipartFormData.append("\(AppDelegate.getUser()._id)".data(using:String.Encoding.utf8)!, withName: "user_id")
                multipartFormData.append("\(self._target._id)".data(using:String.Encoding.utf8)!, withName: "sender_id")
                multipartFormData.append("\(type)".data(using:String.Encoding.utf8)!, withName: "type")
                multipartFormData.append(URL(fileURLWithPath: filePath), withName: "file")
                
        },
            to: Constant.REQ_SENDMESSAGEFILE
            ,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print("register response : ", response)
                        
                        if let result = response.result.value {
                            
                            self.hideLoadingView()
                            
                            let dict = JSON(result)
                            
                            let result_code = dict[Constant.RES_RESULTCODE].intValue
                            
                            if result_code == Constant.CODE_SUCCESS {

                                if self._from == Constant.FROM_READYPLAYLIST {
                                    let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                                    self.navigationController!.popToViewController(viewControllers[viewControllers.count - 4], animated: true)
                                }
                            } else {
                                
                                self.hideLoadingView()
                                self.showAlertDialog(title: R.string.ERROR, message: R.string.UPLOAD_FAIL, positive: R.string.OK, negative: nil)
                            }
                            
                        } else  {
                            
                            self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.hideLoadingView()
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    return
                    
                }
        }
        )
    }
    

    func didTapBack() {
        
        //set read Message
            
        let URL = Constant.REQ_SETREADMSG + "\(AppDelegate.getUser()._id)/\(self._target._id)"
        
        print("set read message url", URL)
        
        Alamofire.request(URL, method:.get)
            .responseJSON { response in
                
        }
        
        self.perform(#selector(goBack), with: nil, afterDelay: 0.5)
    }
    
    func goBack() {
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    func didTapRocket() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        
        let conditionVC = storyboard.instantiateViewController(withIdentifier: "SelectConditionViewController") as! SelectConditionViewController
        conditionVC._senderUser = self._target
        
        self.navigationController?.pushViewController(conditionVC, animated: true)
        
    }

    
    func gotoProfile() {
        
        //got invite decision
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let profileVC = storyboard.instantiateViewController(withIdentifier: "InviteDecideViewController") as! InviteDecideViewController
        profileVC._from = Constant.FROM_CHATTING
        profileVC._selectedUser = _target
        self.navigationController?.pushViewController(profileVC, animated: true)
        
    }
    
    // MARK: Collection view data source (and related) methods
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return _messages.count
    }
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        
        return _messages[indexPath.row]
    }
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        
        print("self.senderId = \(self.senderId), message sender id = \(_messages[indexPath.row].senderId)")
        
        let messageSenderId = _messages[indexPath.row].senderId
        return (messageSenderId == self.senderId) ? outgoingBubble : incomingBubble
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        
        let message = _messages[indexPath.item]
        
        if message.senderId == self.senderId { // 1
            
            cell.textView?.textColor = UIColor.black// 2
            cell.cellBottomLabel.text = getTimeString(date: message.date)
            //cell.cellBottomLabel.textColor = UIColor.gray
            cell.avatarImageView.sd_setImage(with: URL(string: AppDelegate.getUser()._profileUrls[0]), placeholderImage:#imageLiteral(resourceName: "img_user"))
            
        } else {
            cell.textView?.textColor = UIColor.black // 3
            cell.cellBottomLabel.text = getTimeString(date: message.date)
            //cell.cellBottomLabel.textColor = UIColor.gray
            cell.avatarImageView.sd_setImage(with: URL(string: self._target._profileUrls[0]), placeholderImage:#imageLiteral(resourceName: "img_user"))
        }
        
        cell.avatarImageView.layer.borderWidth = 2.0
        cell.avatarImageView.layer.cornerRadius = 20
        cell.avatarImageView.layer.masksToBounds = true
        cell.avatarImageView.layer.borderColor = UIColor.white.cgColor
        
        return cell
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapAvatarImageView avatarImageView: UIImageView!, at indexPath: IndexPath!) {
        
        let message = _messages[indexPath.row]
        
        if message.senderId != self.senderId {
            
            self.gotoProfile()
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAt indexPath: IndexPath!) {
        
        print("bubble did tapped")
        
        let message = _messages[indexPath.row]
            
        guard let mediaData = message.media else {
            return
        }
        
        if mediaData is AsyncPhotoMediaItem {
            // photo message
            let image = (mediaData as! AsyncPhotoMediaItem).asyncImageView.image
            //let image = (mediaData as! AsyncPhotoMediaItem).image
            
            let photoVC = PhotoViewController(image:image!)
            photoVC._from = Constant.FROM_CHATTING
            self.navigationController?.pushViewController(photoVC, animated: true)
            
        } else if mediaData is JSQVideoMediaItem {
            // video message
            let videoURL = (mediaData as! JSQVideoMediaItem).fileURL
            let videoVC = VideoViewController(videoURL:videoURL!)
            videoVC._from = Constant.FROM_CHATTING
            self.navigationController?.pushViewController(videoVC, animated: true)
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapCellAt indexPath: IndexPath!, touchLocation: CGPoint) {
        
        print(_messages[indexPath.item].senderId, senderId)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        return 12
    }
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellBottomLabelAt indexPath: IndexPath!) -> CGFloat {
        return 24
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView?, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString? {
        
        return nil
    }
    
    
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        
        
        self.addMessage(withId: senderId, name: senderDisplayName, date: Date(), text: text)
        
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        
        sendMessage(text: text)
        
        finishSendingMessage()

    }
    
    override func didPressAccessoryButton(_ sender: UIButton) {
        self.inputToolbar.contentView!.textView!.resignFirstResponder()
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
        {
        
            let sheet = UIAlertController(title: "Media messages", message: nil, preferredStyle: .actionSheet)
            
            let photoAction = UIAlertAction(title: "Send photo", style: .default) { (action) in
                /**
                 *  Create fake photo
                 */
                
                self._picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                let kUTTypeImageAnyObject : AnyObject = kUTTypeImage as AnyObject
                self._picker.mediaTypes = [kUTTypeImageAnyObject as! String]
                self.present(self._picker, animated: true, completion: nil)
               
            }

            let videoAction = UIAlertAction(title: "Send video", style: .default) { (action) in
                /**
                 *  Add fake video
                 */
                
                let kUTTypeMovieAnyObject : AnyObject = kUTTypeMovie as AnyObject
                let kUTTypeMpegAnyObject : AnyObject = kUTTypeMPEG2Video as AnyObject
                let kUTTypeVideoAnyObject : AnyObject = kUTTypeVideo as AnyObject
                self._picker.mediaTypes = [kUTTypeMovieAnyObject as! String, kUTTypeMpegAnyObject as! String, kUTTypeVideoAnyObject as! String]
                self.present(self._picker, animated: true, completion: nil)
                
            }

            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            sheet.addAction(photoAction)
            //sheet.addAction(locationAction)
            sheet.addAction(videoAction)
            //sheet.addAction(audioAction)
            sheet.addAction(cancelAction)
            
            self.present(sheet, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            
            //self._imgUrl.image = pickedImage
            _imgPath = saveToFile(image: pickedImage, filePath: Constant.SAVE_ROOT_PATH, fileName: "image.png")
            
            print(_imgPath)
            
            let photoItem = JSQPhotoMediaItem(image: UIImage.init(contentsOfFile: self._imgPath))
            self.addMedia(photoItem!)
            
            self.sendMediaMessage(filePath: _imgPath, type: 1)
            
        }
        
        if let videoURL = info[UIImagePickerControllerMediaURL] as? NSURL {
            _videoPath = videoURL.path!
            print(_videoPath)
            
            let videoItem = self.buildVideoItem()
            
            self.addMedia(videoItem)
            
            self.sendMediaMessage(filePath: _videoPath, type: 2)
        }
        
        dismiss(animated: true, completion: nil)
        
    }

    
    func buildVideoItem() -> JSQVideoMediaItem {
        let videoURL = URL(fileURLWithPath: "file://")
        
        let videoItem = JSQVideoMediaItem(fileURL: videoURL, isReadyToPlay: true)
        
        return videoItem!
    }

    
    func addMedia(_ media:JSQMediaItem) {
        let message = JSQMessage(senderId: self.senderId, displayName: self.senderDisplayName, media: media)
        self._messages.append(message!)
        
        //Optional: play sent sound
        
        self.finishSendingMessage(animated: true)
    }
    
    func sendPhotoMessage() {
        
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        
        finishSendingMessage()
        
    }
    
    func setImageURL(_ url: String, forPhotoMessageWithKey key: String) {
        
        
    }
    
    func onReceiveMessage(message:String, type:Int) {
        
        var newMessage : JSQMessage!
        var newMediaData:JSQMessageMediaData!

        
        if type == ChatViewController.TYPE_TEXT {
            
            self.addMessage(withId: "\(_target._id)", name: "\(_target._name)", text: message)
            self.finishReceivingMessage()
        } else {
        
            switch type {
                
            case ChatViewController.TYPE_IMAGE :
                
                let imageUrl:URL = URL(string: message)!
                let imageData:NSData = NSData(contentsOf: imageUrl)!
                
                let photoItemCopy = JSQPhotoMediaItem(image: UIImage(data: imageData as Data))
                
                if let photoItemCopy = photoItemCopy {
                    
                    photoItemCopy.appliesMediaViewMaskAsOutgoing = false
                   
                }
                
                newMediaData = photoItemCopy
                
                break
                
            case ChatViewController.TYPE_VIDEO :
                
                let videoURL = URL(string: message)
                let videoItemCopy = JSQVideoMediaItem(fileURL: videoURL, isReadyToPlay: true)
                
                
                if let videoItemCopy = videoItemCopy {
                    
                    videoItemCopy.appliesMediaViewMaskAsOutgoing = false
                    
                }
                
                newMediaData = videoItemCopy;
                
                break
                
            default :
                
                break
            }
            
            let currentDate = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            formatter.timeZone = TimeZone(identifier:"UTC")
            let today = formatter.string(from: currentDate)
            let date = formatter.date(from: today)
          
            
            newMessage = JSQMessage(senderId: "\(_target._id)", senderDisplayName : "\(_target._name)", date : date, media:newMediaData);
            self._messages.append(newMessage)
            self.finishReceivingMessage()
        }
    }
    

    
    // MARK: UI and User Interaction
    
    private func setupOutgoingBubble() -> JSQMessagesBubbleImage {
        
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.outgoingMessagesBubbleImage(with: Constant.mainColor);
    }
    
    private func setupIncomingBubble() -> JSQMessagesBubbleImage {
        
        
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        
        let color = Constant.darkGrayColor
        return bubbleImageFactory!.incomingMessagesBubbleImage(with: color)
    }

    
    private func addMessage(withId id: String, name: String, text: String) {
        if let message = JSQMessage(senderId: id, displayName: name, text: text.decodeEmoji) {
            _messages.append(message)
        }
    }
    
    private func addMessage(withId id: String, name: String, date: Date, text: String) {
        
        if let message = JSQMessage(senderId: id, senderDisplayName: name, date: date, text: text.decodeEmoji) {
            _messages.append(message)
        }
    }
    
    private func addPhotoMessage(withId id: String, key: String, mediaItem: JSQPhotoMediaItem) {
        if let message = JSQMessage(senderId: id, displayName: "", media: mediaItem) {
            _messages.append(message)
            
            if (mediaItem.image == nil) {
                photoMessageMap[key] = mediaItem
            }
            
            collectionView.reloadData()
        }
    }
    
    // MARK: UITextViewDelegate methods
    
    override func textViewDidChange(_ textView: UITextView) {
        super.textViewDidChange(textView)
        // If the text is not empty, the user is typing
        //    isTyping = textView.text != ""
    }

    
    func getTimeString(date : Date) -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mm a"
        formatter.timeZone = TimeZone.current
        let dateString = formatter.string(from: date)
        
        return dateString
    }
}

extension ChatViewController {
    
    func showAlertDialog(title: String!, message: String!, positive: String?, negative: String?) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if (positive != nil) {
            
            alert.addAction(UIAlertAction(title: positive, style: .default, handler: nil))
        }
        
        if (negative != nil) {
            
            alert.addAction(UIAlertAction(title: negative, style: .default, handler: nil))
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    func showLoadingView() {
        
        showLoadingViewWithTitle(title: "")
    }
    
    
    func showLoadingViewWithTitle(title: String) {
        
        if title == "" {
            
            ProgressHUD.show()
            
        } else {
            
            ProgressHUD.showWithStatus(string: title)
        }
    }
    
    // hide loading view
    func hideLoadingView() {
        
        ProgressHUD.dismiss()
    }
}
