//
//  ChatListViewController.swift
//  Krepta
//
//  Created by Beautistar on 11/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ChatListViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var tblChatList: UITableView!
    @IBOutlet weak var cvReadyPlayUserList: UICollectionView!
    @IBOutlet weak var lblUnreadCnt: UILabel!
    var w:CGFloat = 0.0
    var h:CGFloat = 0.0

    var _playUserInvites = [InviteEntity]()
    var _chatListUsers = [ChatListUserEntity]()
    var _unreadCnt = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tblChatList.tableFooterView = UIView()
        self.lblUnreadCnt.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        w = self.view.frame.size.width / 4.0 - 15.0
        self.navigationController?.navigationBar.isHidden = false
        
        g_currentVC = self
        
        _unreadCnt = 0
        self.getPlayUsers()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
    }
    
    //MARK: - TableView Datasource & Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self._chatListUsers.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatListCell") as! ChatListCell
        cell.imvPhoto.sd_setImage(with: URL(string: self._chatListUsers[indexPath.row]._chat_user._profileUrls[0]), placeholderImage: #imageLiteral(resourceName: "img_user"))
        cell.lblName.text = self._chatListUsers[indexPath.row]._chat_user._name
        cell.btnProfilePhoto.tag = indexPath.row
        
        cell.lblLastMessage.text = self._chatListUsers[indexPath.row]._message
        
        if _chatListUsers[indexPath.row]._read_status == 0 && _chatListUsers[indexPath.row]._message_type == "received" { //unread
            cell.lblReadStatus.isHidden = false
        } else {
            cell.lblReadStatus.isHidden = true
        }
            
        cell.btnProfilePhoto.addTarget(self, action: #selector(gotoProfile), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {        
        
        let chattingVC = ChatViewController()
        chattingVC._target = self._chatListUsers[indexPath.row]._chat_user
        chattingVC._from = Constant.FROM_CHATLIST
        self.navigationController?.pushViewController(chattingVC, animated: true)
    }   
    
    
    func gotoProfile(_ sender:UIButton) {
        
        //got invite decision
        let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "InviteDecideViewController") as! InviteDecideViewController
        profileVC._from = Constant.FROM_CHATLIST
        profileVC._selectedUser = _chatListUsers[sender.tag]._chat_user
        self.navigationController?.pushViewController(profileVC, animated: true)
        
    }
    
    //MARK: - CollectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self._playUserInvites.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChatListCollectionCell", for: indexPath) as! ChatListCollectionCell
        cell.imvPhoto.sd_setImage(with: URL(string: _playUserInvites[indexPath.row]._inviteUser._profileUrls[0]), placeholderImage: #imageLiteral(resourceName: "img_user"))
        cell.lblName.text = _playUserInvites[indexPath.row]._inviteUser._name
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let conditionVC = self.storyboard?.instantiateViewController(withIdentifier: "UserConditionsViewController") as! UserConditionsViewController
        conditionVC._from = Constant.FROM_READYPLAYLIST
        conditionVC._selectedInvite = _playUserInvites[indexPath.row]
        self.navigationController?.pushViewController(conditionVC, animated: true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func getPlayUsers() {
        
        self._playUserInvites.removeAll()
        self._chatListUsers.removeAll()
        
        self.tblChatList.reloadData()
        self.cvReadyPlayUserList.reloadData()

        
        showLoadingView()        
        let URL = Constant.REQ_GETPLAY_CHAT_USERS + "\(AppDelegate.getUser()._id)/"
        print(URL)
        
        Alamofire.request(URL, method:.get)
            .responseJSON { response in
                
                if response.result.isFailure {
                    
                    self.hideLoadingView()
                    self.showToast(R.string.CONNECT_FAIL)
                    return
                }
                
                if let result = response.result.value  {
                    
                    self.hideLoadingView()
                    
                    print(result)
                    
                    let dict = JSON(result)
                    
                    print("playusers result", dict)
                    
                    print(dict)
                    
                    let result_code = dict[Constant.RES_RESULTCODE].intValue
                    
                    if result_code == Constant.CODE_SUCCESS {
                        
                        //-- ready play users
                        let inviteInfos = dict[Constant.RES_PLAYUSER_INVITE].arrayValue
                        
                        for i in 0..<inviteInfos.count {
                            
                            let _invite = InviteEntity()
                            
                            _invite._inviteUser._id = inviteInfos[i]["invite_user"]["id"].intValue
                            _invite._inviteUser._username = inviteInfos[i]["invite_user"]["username"].stringValue
                            _invite._inviteUser._name = inviteInfos[i]["invite_user"]["name"].stringValue
                            _invite._inviteUser._email = inviteInfos[i]["invite_user"]["email"].stringValue
                            _invite._inviteUser._age = inviteInfos[i]["invite_user"]["age"].intValue
                            _invite._inviteUser._gender = inviteInfos[i]["invite_user"]["gender"].stringValue
                            
                            let photo_urls = inviteInfos[i]["invite_user"]["photo_url"].arrayValue
                            _invite._inviteUser._profileUrls.removeAll()
                            
                            for j in 0..<photo_urls.count {
                                
                                _invite._inviteUser._profileUrls.append(photo_urls[j].stringValue)
                            }
                            
                            _invite._invite_condition = inviteInfos[i]["invite_condition"].stringValue
                            _invite._reply_condition = inviteInfos[i]["reply_condition"].stringValue
                            _invite._status = inviteInfos[i]["status"].intValue
                            _invite._request_userid = inviteInfos[i]["request_user"].intValue
                            self._playUserInvites.append(_invite)
                            
                        }
                        
                        //--  chat list users
                        let chatListUsers = dict[Constant.RES_CHATLISTUSER].arrayValue
                        
                        for i in 0..<chatListUsers.count {
                            
                            let _chatUser = ChatListUserEntity()
                            
                            _chatUser._chat_user._id = chatListUsers[i]["chat_user"]["id"].intValue
                            _chatUser._chat_user._username = chatListUsers[i]["chat_user"]["username"].stringValue
                            _chatUser._chat_user._name = chatListUsers[i]["chat_user"]["name"].stringValue
                            _chatUser._chat_user._email = chatListUsers[i]["chat_user"]["email"].stringValue
                            _chatUser._chat_user._age = chatListUsers[i]["chat_user"]["age"].intValue
                            _chatUser._chat_user._gender = chatListUsers[i]["chat_user"]["gender"].stringValue
                            _chatUser._chat_user._status = chatListUsers[i]["status"].stringValue
                            _chatUser._read_status = chatListUsers[i]["read_status"].intValue
                            _chatUser._message_type = chatListUsers[i]["message_type"].stringValue
                            _chatUser._content_type = chatListUsers[i]["content_type"].intValue
                            
                            if _chatUser._read_status == 0 && _chatUser._message_type == "received" { //unread
                                self._unreadCnt += 1
                            }
                            
                            
                            switch chatListUsers[i]["content_type"].intValue {
                                
                                case 0 :
                                    _chatUser._message = chatListUsers[i]["message"].stringValue.decodeEmoji
                                    break
                                
                                case 1:
                                    if chatListUsers[i]["message_type"] == "received" {
                                        _chatUser._message = "Image received"
                                    } else {
                                        _chatUser._message = "Image sent"
                                    }
                                    break
                                
                                default :
                                    if chatListUsers[i]["message_type"] == "received" {
                                        _chatUser._message = "Video received"
                                    } else {
                                        _chatUser._message = "Video sent"
                                    }
                            }
                            
                            let photo_urls = chatListUsers[i]["chat_user"]["photo_url"].arrayValue
                            _chatUser._chat_user._profileUrls.removeAll()
                            for j in 0..<photo_urls.count {
                                
                                _chatUser._chat_user._profileUrls.append(photo_urls[j].stringValue)
                            }
                            
                            self._chatListUsers.append(_chatUser)
                        }
                        
                        self.tblChatList.reloadData()
                        self.cvReadyPlayUserList.reloadData()
                        
                        if self._unreadCnt == 0 {
                            self.lblUnreadCnt.isHidden = true
                        } else {
                            self.lblUnreadCnt.isHidden = false
                            self.lblUnreadCnt.text = "\(self._unreadCnt)"
                        }
                    }
                }
        }
    }
}

