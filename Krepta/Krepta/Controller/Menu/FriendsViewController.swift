//
//  FriendsViewController.swift
//  Krepta
//
//  Created by Beautistar on 6/9/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class FriendsViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    weak var delegate: LeftMenuProtocol?
    @IBOutlet weak var tblFriendList: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var _userInfos = [UserEntity]()
    var _filteredData = [UserEntity]()
    var _allUsers = [UserEntity]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tblFriendList.tableFooterView = UIView()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.removeNavigationBarItem()
        //self.setNavigationBarItem()
        
        getFriend()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil, completion: { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            guard let vc = (self.slideMenuController()?.mainViewController as? UINavigationController)?.topViewController else {
                return
            }
            if vc.isKind(of: FriendsViewController.self)  {
                self.slideMenuController()?.removeLeftGestures()
                self.slideMenuController()?.removeRightGestures()
            }
        })
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        delegate?.changeViewController(LeftMenu.main)
    }
    
    @IBAction func chatAction(_ sender: Any) {
        
        let chatListVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatListViewController") as! ChatListViewController
        self.navigationController?.pushViewController(chatListVC, animated: true)
    }
    
    // MARK: - getFriend API 
    
    func getFriend() {
        
        _userInfos.removeAll()
        self.tblFriendList.reloadData()
        
        showLoadingView()
        
        let URL = Constant.REQ_GETFRIEND + "\(AppDelegate.getUser()._id)/"
        print(URL)
        
        Alamofire.request(URL, method:.get)
            .responseJSON { response in
                
                if response.result.isFailure {
                    
                    self.hideLoadingView()
                    self.showToast(R.string.CONNECT_FAIL)
                    return
                }
                
                if let result = response.result.value  {
                    
                    self.hideLoadingView()
                    
                    let dict = JSON(result)
                    
                    print("get friends result", dict)
                    
                    let result_code = dict[Constant.RES_RESULTCODE].intValue
                    
                    if result_code == Constant.CODE_SUCCESS {
                        
                        //TODO :
                        let userInfos = dict[Constant.RES_USERINFO].arrayValue
                        print(userInfos.count)
                        for i in 0..<userInfos.count {
                            
                            let _user = UserEntity()
                            
                            _user._id = userInfos[i]["id"].intValue
                            _user._username = userInfos[i]["username"].stringValue
                            _user._name = userInfos[i]["name"].stringValue
                            _user._email = userInfos[i]["email"].stringValue
                            _user._age = userInfos[i]["age"].intValue
                            _user._gender = userInfos[i]["gender"].stringValue
                            _user._latitude = Double(userInfos[i]["latitude"].floatValue)
                            _user._longitude = Double(userInfos[i]["longitude"].floatValue)
                            _user._condition = userInfos[i]["condition"].stringValue
                            _user._status = userInfos[i]["status"].stringValue
                            let photo_urls = userInfos[i]["photo_url"].arrayValue
                            _user._profileUrls.removeAll()
                            for j in 0..<photo_urls.count {
                                
                                _user._profileUrls.append(photo_urls[j].stringValue)
                            }
                            
                            self._userInfos.append(_user)
                            self._allUsers.append(_user)
                        }
                        
                        self.tblFriendList.reloadData()

                    }
                }
        }
        
    }
    
    // MARK: - SearchBar delegate
    
    // This method updates filteredData based on the text in the Search Box
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        _filteredData.removeAll()
        let keyword = searchBar.text!.lowercased()
        if keyword.isEmpty{
            _filteredData.append(contentsOf:_allUsers)
        } else {
            
            for one in _userInfos {
                
                let text = "\(one._username)".lowercased()
                if text.range(of:keyword) != nil {
                    _filteredData.append(one)
                }
            }
        }
        
        _userInfos.removeAll()
        _userInfos.append(contentsOf: _filteredData)

        self.tblFriendList.reloadData()
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
        _userInfos.append(contentsOf: _allUsers)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
    }
    
    // MARK: - TableView delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return _userInfos.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendCell") as! FriendCell
        cell.btnProfile.tag = indexPath.row
        cell.imvUserPhoto.sd_setImage(with: URL(string: _userInfos[indexPath.row]._profileUrls[0]), placeholderImage: #imageLiteral(resourceName: "img_user"))
        cell.lblUserName.text = _userInfos[indexPath.row]._username
        cell.btnProfile.addTarget(self, action: #selector(profileTapped(_:)), for: .touchUpInside)
        return cell
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //let chattingVC = self.storyboard?.instantiateViewController(withIdentifier: "ChattingViewController") as! ChattingViewController
        let chattingVC = ChatViewController()
        chattingVC._target = self._userInfos[indexPath.row]
        chattingVC._from = Constant.FROM_MYFRIEND
        self.navigationController?.pushViewController(chattingVC, animated: true)

    }
    
    @objc func profileTapped(_ sender:UIButton) {
        
        print(sender.tag)
        let inviteDecisionVC = self.storyboard?.instantiateViewController(withIdentifier: "InviteDecideViewController") as! InviteDecideViewController
        inviteDecisionVC._from = Constant.FROM_MYFRIEND
        inviteDecisionVC._selectedUser = _userInfos[sender.tag]
        self.navigationController?.pushViewController(inviteDecisionVC, animated: true)
    }
}
