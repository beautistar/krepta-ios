//
//  NewFriendViewController.swift
//  Krepta
//
//  Created by Beautistar on 6/9/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class NewFriendViewController: BaseViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource {
    
    weak var delegate: LeftMenuProtocol?
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var cvAddedList: UICollectionView!
    @IBOutlet weak var tblSearchedUserList: UITableView!
    @IBOutlet weak var lblMyUsername: UILabel!
    
    var w:CGFloat = 0.0
    var h:CGFloat = 0.0
    
    var _userInfos = [UserEntity]()
    var _addedList = [UserEntity]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lblMyUsername.text = AppDelegate.getUser()._username
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.removeNavigationBarItem()
        
         self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Space Age", size: 23)!, NSForegroundColorAttributeName: UIColor.white]
        
        w = self.view.frame.size.width
        h = 70
        
        tblSearchedUserList.tableFooterView = UIView()
        
        tblSearchedUserList.isHidden = true
        
        getRequestedFriend()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil, completion: { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            guard let vc = (self.slideMenuController()?.mainViewController as? UINavigationController)?.topViewController else {
                return
            }
            if vc.isKind(of: NewFriendViewController.self)  {
                self.slideMenuController()?.removeLeftGestures()
                self.slideMenuController()?.removeRightGestures()
            }
        })
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        delegate?.changeViewController(LeftMenu.main)
    }
    
    @IBAction func gotoChat(_ sender: Any) {
        
        let chatListVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatListViewController") as! ChatListViewController
        self.navigationController?.pushViewController(chatListVC, animated: true)
    }
    
    // MARK: - Get Requested Friend, Search Friend Add Freiend API
    
    func getRequestedFriend() {
        
        showLoadingView()        

        let URL = Constant.REQ_GETREQUESTEDFRIEND + "\(AppDelegate.getUser()._id)/"
       
        print(URL)
        
        Alamofire.request(URL, method:.get)
            .responseJSON { response in
                
                if response.result.isFailure {
                    
                    self.hideLoadingView()
                    self.showToast(R.string.CONNECT_FAIL)
                    return
                }
                
                if let result = response.result.value  {
                    
                    self.hideLoadingView()
                    
                    let dict = JSON(result)
                    
                    let result_code = dict[Constant.RES_RESULTCODE].intValue
                    
                    if result_code == Constant.CODE_SUCCESS {
                        
                        //TODO :
                        let userInfos = dict[Constant.RES_USERINFO].arrayValue
                        print(userInfos.count)
                        for i in 0..<userInfos.count {
                            
                            let _user = UserEntity()
                            
                            _user._id = userInfos[i]["id"].intValue
                            _user._username = userInfos[i]["username"].stringValue
                            _user._email = userInfos[i]["email"].stringValue
                            _user._name = userInfos[i]["name"].stringValue
                            _user._age = userInfos[i]["age"].intValue
                            _user._gender = userInfos[i]["gender"].stringValue
                            _user._latitude = Double(userInfos[i]["latitude"].floatValue)
                            _user._longitude = Double(userInfos[i]["longitude"].floatValue)
                            _user._distance = Double(userInfos[i]["distance"].intValue)                            
                            _user._condition = userInfos[i]["condition"].stringValue
                            _user._status = userInfos[i]["status"].stringValue
                            let photo_urls = userInfos[i]["photo_url"].arrayValue
                            _user._profileUrls.removeAll()
                            for j in 0..<photo_urls.count {
                                
                                _user._profileUrls.append(photo_urls[j].stringValue)
                            }
                            
                            self._addedList.append(_user)
                        }
                        
                        self.cvAddedList.reloadData()
                    }
                }
        }
    }
    
    func searchFreiend() {
        
        _userInfos.removeAll()
        _addedList.removeAll()
        
        tblSearchedUserList.reloadData()
        cvAddedList.reloadData()
        
        showLoadingView()
        
        let search_string = searchBar.text!
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                multipartFormData.append("\(AppDelegate.getUser()._id)".data(using:String.Encoding.utf8)!, withName: "user_id")
                multipartFormData.append("\(search_string)".data(using:String.Encoding.utf8)!, withName: "search_string")
        },
            to: Constant.REQ_SEARCHUSERS,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print("search friend response : ", response)
                        self.hideLoadingView()
                        
                        if let result = response.result.value {
                            
                            let dict = JSON(result)
                            
                            let result_code = dict[Constant.RES_RESULTCODE].intValue
                            
                            if result_code == Constant.CODE_SUCCESS {
                                
                                //TODO : success
                                
                                let userInfos = dict[Constant.RES_USERINFO].arrayValue
                                print(userInfos.count)
                                for i in 0..<userInfos.count {
                                    
                                    let _user = UserEntity()
                                    
                                    _user._id = userInfos[i]["id"].intValue
                                    _user._username = userInfos[i]["username"].stringValue
                                    _user._name = userInfos[i]["name"].stringValue
                                    _user._email = userInfos[i]["email"].stringValue
                                    _user._age = userInfos[i]["age"].intValue
                                    _user._gender = userInfos[i]["gender"].stringValue
                                    _user._latitude = Double(userInfos[i]["latitude"].floatValue)
                                    _user._longitude = Double(userInfos[i]["longitude"].floatValue)
                                    _user._distance = Double(userInfos[i]["distance"].intValue)
                                    _user._condition = userInfos[i]["condition"].stringValue
                                    _user._status = userInfos[i]["status"].stringValue
                                    let photo_urls = userInfos[i]["photo_url"].arrayValue
                                    _user._profileUrls.removeAll()
                                    for j in 0..<photo_urls.count {
                                        
                                        _user._profileUrls.append(photo_urls[j].stringValue)
                                    }
                                    
                                    self._userInfos.append(_user)
                                }
                                
                                self.tblSearchedUserList.reloadData()

                            }
                            
                        } else  {
                            
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    
                    self.hideLoadingView()
                    
                    return
                }
        }
        )
    }
    
    func addFreiend(sender_id:Int, index: Int) {
        
        showLoadingView()
        
        print("add friend user and sender id", AppDelegate.getUser()._id, sender_id)
        
        let URL = Constant.REQ_ADDFRIEND + "\(AppDelegate.getUser()._id)/" + "\(sender_id)/"
        print(URL)
        
        Alamofire.request(URL, method:.get)
            .responseJSON { response in
                
                if response.result.isFailure {
                    
                    self.hideLoadingView()
                    self.showToast(R.string.CONNECT_FAIL)
                    return
                }
                
                if let result = response.result.value  {
                    
                    self.hideLoadingView()
                    
                    let dict = JSON(result)
                    
                    let result_code = dict[Constant.RES_RESULTCODE].intValue
                    
                    if result_code == Constant.CODE_SUCCESS {
                     
                        //TODO :
                        print("success to added")
                        self._addedList.remove(at: index)
                        self.cvAddedList.reloadData()
                    }
                }
        }

    }
    
    // MARK: - SearchBar delegate
    
    // This method updates filteredData based on the text in the Search Box
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        /*
         filteredData = searchText.isEmpty ? data : data.filter { (item: String) -> Bool in
         // If dataItem matches the searchText, return true to include it
         return item.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
         }
         
         tableView.reloadData()
         */
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
        tblSearchedUserList.isHidden = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
        tblSearchedUserList.isHidden = true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        self.searchFreiend()
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
    }
    
    //MARK: - TableView
    // MARK: - TableView delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self._userInfos.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendCell") as! FriendCell
        cell.imvUserPhoto.sd_setImage(with: URL(string: _userInfos[indexPath.row]._profileUrls[0]), placeholderImage: #imageLiteral(resourceName: "img_user"))
        cell.lblUserName.text = _userInfos[indexPath.row]._username
        cell.btnAddUser.tag = indexPath.row
        cell.btnAddUser.addTarget(self, action: #selector(sendFriendRequest(_:)), for: .touchUpInside)
        
        return cell
    }    

    @objc func sendFriendRequest(_ sender:UIButton) {
        
        showLoadingView()
        
        print("add friend user and sender id", AppDelegate.getUser()._id, _userInfos[sender.tag]._id)
        
        let URL = Constant.REQ_ADDFRIEND + "\(AppDelegate.getUser()._id)/" + "\(_userInfos[sender.tag]._id)/"
        print(URL)
        
        Alamofire.request(URL, method:.get)
            .responseJSON { response in
                
                if response.result.isFailure {
                    
                    self.hideLoadingView()
                    self.showToast(R.string.CONNECT_FAIL)
                    return
                }
                
                if let result = response.result.value  {
                    
                    self.hideLoadingView()
                    
                    let dict = JSON(result)
                    
                    let result_code = dict[Constant.RES_RESULTCODE].intValue
                    
                    if result_code == Constant.CODE_SUCCESS {
                        
                        //TODO :
                        print("success to request sent")
                        
                        self._userInfos.remove(at: sender.tag)
                        self.tblSearchedUserList.reloadData()
                        
                    }
                }
        }


        
    }
    
    //MARK: - CollectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return _addedList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewFriendCollectionCell", for: indexPath) as! NewFriendCollectionCell
        cell.imvUserPhoto.sd_setImage(with: URL(string: _addedList[indexPath.row]._profileUrls[0]), placeholderImage: #imageLiteral(resourceName: "img_user"))
        cell.lblUserName.text = _addedList[indexPath.row]._username
        cell.btnAccept.tag = indexPath.row
        cell.btnAccept.addTarget(self, action: #selector(acceptFriend(_:)), for: .touchUpInside)
        cell.btnReject.tag = indexPath.row
        cell.btnReject.addTarget(self, action: #selector(rejectFriend(_:)), for: .touchUpInside)
        return cell
    }
    
    
    //MARK: - Accept Friend request API
    
    func acceptFriend(_ sender:UIButton) {
        
        showLoadingView()
        
        print("accept friend user and sender id", AppDelegate.getUser()._id, _addedList[sender.tag]._id)
        
        let URL = Constant.REQ_ACCEPTFRIEND + "\(AppDelegate.getUser()._id)/" + "\(_addedList[sender.tag]._id)/"
        print(URL)
        
        Alamofire.request(URL, method:.get)
            .responseJSON { response in
                
                if response.result.isFailure {
                    
                    self.hideLoadingView()
                    self.showToast(R.string.CONNECT_FAIL)
                    return
                }
                
                if let result = response.result.value  {
                    
                    self.hideLoadingView()
                    
                    let dict = JSON(result)
                    
                    let result_code = dict[Constant.RES_RESULTCODE].intValue
                    
                    if result_code == Constant.CODE_SUCCESS {
                        
                        //TODO :
                        print("success to accept friend")
                        
                        self._addedList.remove(at: sender.tag)
                        self.cvAddedList.reloadData()
                    }
                }
        }
    }
    
    func rejectFriend(_ sender: UIButton) {
        
        _addedList.remove(at: sender.tag)
        cvAddedList.reloadData()
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        tblSearchedUserList.isHidden = true
    }
}


extension NewFriendViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout:UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: CGFloat(w), height: CGFloat(h))
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

}

