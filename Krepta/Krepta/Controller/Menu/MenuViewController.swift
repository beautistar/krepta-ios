//
//  MenuViewController.swift
//  Krepta
//
//  Created by Beautistar on 6/9/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import Alamofire

enum LeftMenu: Int {
    case main = 0
    case friends
    case new_friends
    //case setting
    case signout
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}


class MenuViewController: BaseViewController, LeftMenuProtocol{
    
    @IBOutlet weak var btnInvite: UIButton!
    @IBOutlet weak var tblMenu: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
   
    
    //var menuNames = ["Friends", "New Friends", "Settings", "Sign Out"]
    //var menuIcons = ["ic_friends", "ic_new_friend", "ic_setting", "ic_roket_black"]
    var menuNames = ["Friends", "New Friends", "Sign Out"]
    var menuIcons = ["ic_friends", "ic_new_friend", "ic_roket_black"]
    
    var mainViewController: UIViewController!
    var friendViewController: UIViewController!
    var newFriendViewController: UIViewController!
    //var settingViewController: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {       
        
        tblMenu.tableFooterView = UIView()
        
        btnInvite.layer.masksToBounds = false;
        btnInvite.layer.shadowColor = UIColor.black.cgColor
        btnInvite.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        btnInvite.layer.shadowOpacity = 0.5
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let friendViewController = storyboard.instantiateViewController(withIdentifier: "FriendsViewController") as! FriendsViewController
        friendViewController.delegate = self
        self.friendViewController = UINavigationController(rootViewController: friendViewController)
        
        let newFriendViewController = storyboard.instantiateViewController(withIdentifier: "NewFriendViewController") as! NewFriendViewController
        newFriendViewController.delegate = self
        self.newFriendViewController = UINavigationController(rootViewController: newFriendViewController)
        /*
        let settingViewController = storyboard.instantiateViewController(withIdentifier: "SettingViewController") as! SettingViewController
        settingViewController.delegate = self
        self.settingViewController = UINavigationController(rootViewController: settingViewController)
        */
        let mainTabViewController = storyboard.instantiateViewController(withIdentifier: "MainTabViewController") as! MainTabViewController
        self.mainViewController = UINavigationController(rootViewController: mainTabViewController)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        lblTitle.font = UIFont(name: "Space Age", size: 23)
        lblTitle.tintColor = UIColor.white
    }
    
    func changeViewController(_ menu: LeftMenu) {
        
        print(menu);
        
        switch menu {
            
        case .main:
            self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
        
        case .friends:
            self.slideMenuController()?.changeMainViewController(self.friendViewController, close: true)
        case .new_friends:
            self.slideMenuController()?.changeMainViewController(self.newFriendViewController, close: true)
        /*
        case .setting:
            self.slideMenuController()?.changeMainViewController(self.settingViewController, close: true)
        */
        default : /// signout
            
            signout()
            
        }
    }
    
    func signout() {
        
        deleteToken()
        
        //initialize user 
        let user = UserEntity();
        AppDelegate.setUser(user: user)
        
        Defaults[.id] = AppDelegate.getUser()._id
        Defaults[.name] = AppDelegate.getUser()._name
        Defaults[.username] = AppDelegate.getUser()._username
        Defaults[.email] = AppDelegate.getUser()._email
        Defaults[.age] = AppDelegate.getUser()._age
        Defaults[.gender] = AppDelegate.getUser()._gender
        Defaults[.condition] = AppDelegate.getUser()._condition
        Defaults[.status] = AppDelegate.getUser()._status
        Defaults[.photo_urls] = AppDelegate.getUser()._profileUrls
        Defaults[.condition] = AppDelegate.getUser()._condition
        
        FBAlbum.removeAll()
        
        UserDefaults.standard.remove("fb_album")
        
        
        /// for open explaination popviewcontroller when just login
        _loggedIn = false
        Defaults[.isRegister] = false
        
        let loginNav = self.storyboard?.instantiateViewController(withIdentifier: "LoginNav") as! UINavigationController
        
        UIApplication.shared.keyWindow?.rootViewController = loginNav
        
    }
    
    func deleteToken() {
        
        if AppDelegate.getUser()._id == 0 {
            return
        }
        
        let token = "unknown"
            
        let URL = Constant.REQ_REGISTERTOKEN + "\(AppDelegate.getUser()._id)/\(token)"
        
        Alamofire.request(URL, method:.get)
            .responseJSON { response in
        }        
    }
    
    @IBAction func shareAction(_ sender: Any) {
        
        // text to share
        let text = "Hello everyone, Please user Krepta(Comming soon)."
        
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook, UIActivityType.postToTwitter ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
        
    }
}

extension MenuViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = LeftMenu(rawValue: indexPath.row+1) {
            self.changeViewController(menu)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tblMenu == scrollView {
            
        }
    }
}

extension MenuViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuItemCell") as! MenuItemCell
        
        cell.setData(menuIcons[indexPath.row], menuName: menuNames[indexPath.row])
        return cell
    }
    
}

