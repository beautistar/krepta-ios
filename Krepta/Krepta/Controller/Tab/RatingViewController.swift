//
//  RatingViewController.swift
//  Krepta
//
//  Created by Beautistar on 6/9/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class RatingViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblRate: UITableView!
    var rating:Bool = true
    var count:Int = 0
    
    var _rates = [RateEntity]()
    
    var _fRates = [RateEntity]()
    
    
    var _from = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        
        getRate()
    }
    
    func initView() {
        
        tblRate.tableFooterView = UIView()
        
    }
    
    @IBAction func switchAction(_ sender: Any) {
        
        let switchButotn = sender as! UIButton
        
        if switchButotn.tag == 0 {
            
            rating = true
        } else {
            rating = false
        }
        
        filterRate()
    }
    
    func filterRate() {
        
        _fRates.removeAll()
        
        if rating {
            for rate in _rates {
                if rate._is_rating == 1 {
                    _fRates.insert(rate, at: 0)
                }
            }
        } else {
            for rate in _rates {
                if rate._is_rated == 1 {
                    _fRates.insert(rate, at: 0)
                }
            }
        }
        
        tblRate.reloadData()
    }
    
    // MARK: - GetRate API
    
    func getRate() {
        
        showLoadingView()
        _rates.removeAll()
        tblRate.reloadData()

        let URL = Constant.REQ_GETRATE + "\(AppDelegate.getUser()._id)/"
        
        print(URL)
        
        Alamofire.request(URL, method:.get)
            .responseJSON { response in
                
                if response.result.isFailure {
                    
                    self.hideLoadingView()
                    self.showToast(R.string.CONNECT_FAIL)
                    return
                }
                
                if let result = response.result.value  {
                    
                    self.hideLoadingView()
                    
                    print("get rate respinse", result)
                    let dict = JSON(result)
                    
                    let result_code = dict[Constant.RES_RESULTCODE].intValue
                    
                    if result_code == Constant.CODE_SUCCESS {
                        
                        let rates = dict["rate_users"].arrayValue
                        
                        for rate in rates {
                            
                            let _rate = RateEntity()
                            _rate._challenge_id = rate["challenge_id"].intValue
                            _rate._rate_user._id = rate["id"].intValue
                            _rate._rate_user._username = rate["username"].stringValue
                            _rate._rate_user._name = rate["name"].stringValue
                            _rate._rate_user._email = rate["email"].stringValue
                            _rate._rate_user._age = rate["age"].intValue
                            _rate._rate_user._gender = rate["gender"].stringValue                            
                            _rate._rate_user._latitude = Double(rate["latitude"].floatValue)
                            _rate._rate_user._longitude = Double(rate["longitude"].floatValue)
                            _rate._rate_user._distance = Double(rate["distance"].intValue)
                            _rate._rate_user._status = rate["status"].stringValue                            
                            let photo_urls = rate["photo_url"].arrayValue
                            _rate._rate_user._profileUrls.removeAll()
                            for j in 0..<photo_urls.count {
                                _rate._rate_user._profileUrls.append(photo_urls[j].stringValue)
                            }
                            _rate._is_rating = rate["is_rating"].intValue
                            _rate._is_rated = rate["is_rated"].intValue
                            _rate._rating_score = Double(rate["rating_score"].floatValue)
                            _rate._rated_score = Double(rate["rated_score"].floatValue)
                            
                            self._rates.append(_rate)
                        }
                        
                        self.filterRate()
                
                    }
                }
        }
    }
    
    
    //MARK: - TableView delegate & datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return _fRates.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RateListCell") as! RateListCell
        
        cell.btnCamera.tag = indexPath.row
        cell.btnCamera.addTarget(self, action: #selector(gotoCamera(_:)), for: .touchUpInside)
        cell.imvPhoto.sd_setImage(with: URL(string: _fRates[indexPath.row]._rate_user._profileUrls[0]), placeholderImage: #imageLiteral(resourceName: "img_user"))
        cell.lblName.text = _fRates[indexPath.row]._rate_user._name
        
        if rating {
            
            cell.lblRate.text = String(format: "%.1f", _fRates[indexPath.row]._rating_score)
            cell.ratingView.rating = _fRates[indexPath.row]._rating_score
            
            if _fRates[indexPath.row]._rating_score == 0.0 && _from != Constant.FROM_PROFILE{
                cell.lblWaiting.isHidden = false
                cell.btnCamera.isHidden = false
            } else {
                cell.lblWaiting.isHidden = true
                cell.btnCamera.isHidden = true
            }
            
        } else {
            
            cell.lblRate.text = String(format: "%.1f", _fRates[indexPath.row]._rated_score)
            cell.ratingView.rating = _fRates[indexPath.row]._rated_score
            
            if _fRates[indexPath.row]._rated_score == 0.0 && _from != Constant.FROM_PROFILE {
                cell.lblWaiting.isHidden = false
                cell.btnCamera.isHidden = false
            } else {
                cell.lblWaiting.isHidden = true
                cell.btnCamera.isHidden = true
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if _from != Constant.FROM_PROFILE {
           
            if (rating && _fRates[indexPath.row]._rating_score == 0) || (!rating && _fRates[indexPath.row]._rated_score == 0) {
            
                let setRateVC = self.storyboard?.instantiateViewController(withIdentifier: "SetRateViewController") as! SetRateViewController
                setRateVC._target_user = _fRates[indexPath.row]._rate_user
                setRateVC._challenge_id = _fRates[indexPath.row]._challenge_id
                self.navigationController?.pushViewController(setRateVC, animated: true)
            }
        }
    }
    
    func gotoCamera(_ sender: UIButton) {
        
        print(sender.tag)
        print("camera button tapped", sender.tag)
        
        let recordVC = self.storyboard?.instantiateViewController(withIdentifier: "RecordViewController") as! RecordViewController
        recordVC._from = Constant.FROM_RATE
        recordVC._sender_id = _fRates[sender.tag]._rate_user._id
        self.navigationController?.pushViewController(recordVC, animated: true)
    
    }
}
