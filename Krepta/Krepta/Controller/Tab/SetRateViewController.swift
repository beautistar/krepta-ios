//
//  SetRateViewController.swift
//  Krepta
//
//  Created by Beautistar on 12/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import Cosmos

class SetRateViewController: BaseViewController, UITextFieldDelegate{

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var tfComment: UITextField!
    
    var _target_user = UserEntity()
    var _challenge_id = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initView()
    }
    
    func initView() {
        rateView.settings.fillMode = .half
        
        lblTitle.text = "Rate " + _target_user._name + " Now"
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doneAction(_ sender: Any) {
    
        if rateView.rating != 0 && !(tfComment.text?.isEmpty)! && tfComment.text?.length != 0 {
            setRate()
        } else {
            self.showAlertDialog(title: nil, message: "Set rate options.", positive: R.string.OK, negative: nil)
        }
    }
    
    
    func setRate() {
        
        showLoadingView()
        
        let comment = tfComment.text!
        let rate = rateView.rating
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                multipartFormData.append("\(self._challenge_id)".data(using:String.Encoding.utf8)!, withName: "challenge_id")
            multipartFormData.append("\(AppDelegate.getUser()._id)".data(using:String.Encoding.utf8)!, withName: "user_id")
                multipartFormData.append("\(self._target_user._id)".data(using:String.Encoding.utf8)!, withName: "sender_id")
                multipartFormData.append("\(rate)".data(using:String.Encoding.utf8)!, withName: "rate")
                multipartFormData.append("\(comment)".data(using:String.Encoding.utf8)!, withName: "comment")
                
        },
            to: Constant.REQ_SETRATE,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print("set rate response : ", response)
                        self.hideLoadingView()
                        
                        if let result = response.result.value {
                            
                            let dict = JSON(result)
                            
                            let result_code = dict[Constant.RES_RESULTCODE].intValue
                            
                            if result_code == Constant.CODE_SUCCESS {
                                
                                //TODO : success
                                
                                self.navigationController?.popViewController(animated: true)
                            }
                            
                        } else  {
                            
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    
                    self.hideLoadingView()
                    
                    return
                }
        }
        )
    }
    
    //MAEK: - TextField return key delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        return true
    }
}
