//
//  InviteDecideViewController.swift
//  Krepta
//
//  Created by Beautistar on 14/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//
//
// It is invite view controller with Invite user's profile
// Works same as User profile view controller 
//
//



import UIKit

class InviteDecideViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate {
    
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet weak var lblChallenge: UILabel!
    @IBOutlet weak var lblConditionHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var lblCondtionTitleHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var lblCondtionTitle: UILabel!
    @IBOutlet weak var tvStatus: UITextView!
    
    var w:CGFloat = 0.0
    var h:CGFloat = 0.0
    var _currentIndexPath:Int = 0
    var _indexPath_:IndexPath!
    var _count:Int = 0
    
    var _from:Int = 0
    
    var _selectedUser = UserEntity()
    var _challenge:String = ""

    @IBOutlet weak var cvProfile: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.layoutIfNeeded()
        w = self.view.frame.size.width
        h = w
        _count = _selectedUser._profileUrls.count
        
        if _from == Constant.FROM_INVITES {
            
            isReply = true
            
            btnYes.isHidden = false
            btnNo.isHidden = false
            lblConditionHeightConstrain.constant = 50
            lblCondtionTitle.isHidden = false
            
            lblChallenge.text = _challenge
        
        } else {
            
            isReply = false
            btnYes.isHidden = true
            btnNo.isHidden = true
            lblConditionHeightConstrain.constant = 0
            lblCondtionTitle.isHidden = true
            
            lblChallenge.text = _selectedUser._condition
        }
        
        // set user infomation
        tvStatus.text = _selectedUser._status
        
        cvProfile.reloadData()
        
        _inviteViewed = true
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        //isReply = true
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func chatAction(_ sender: Any) {
        
        let chatListVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatListViewController") as! ChatListViewController
        self.navigationController?.pushViewController(chatListVC, animated: true)
    }
    
    @IBAction func gotoRate(_ sender: Any) {
        
        let reatingVC = self.storyboard?.instantiateViewController(withIdentifier: "RatingViewController") as! RatingViewController
        reatingVC._from = Constant.FROM_PROFILE
        self.navigationController?.pushViewController(reatingVC, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        _inviteViewed = false
        let sendReplyVC = segue.destination as! SelectConditionViewController
        sendReplyVC._senderUser = self._selectedUser
        
    }
    
    //MARK: - CollectionView Delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        return _count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileCollectionCell", for: indexPath) as! ProfileCollectionCell
        
        cell.imvProfile.sd_setImage(with: URL(string: _selectedUser._profileUrls[indexPath.row]))
        cell.lblNameAge.text = String(format: "%@, %d", _selectedUser._name, _selectedUser._age)
       
        _indexPath_ = indexPath
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        _currentIndexPath = Int(self.cvProfile.contentOffset.x / self.cvProfile.frame.size.width)
    }

    @IBAction func leftAction(_ sender: Any) {
        
        if cvProfile.contentOffset.x > 0 {
            
            print(cvProfile.contentOffset.x)
            let width = cvProfile.frame.size.width
            
            if cvProfile.indexPathsForVisibleItems.count > 0 {
                let currentIndex = cvProfile.indexPathsForVisibleItems[0].row
                if currentIndex > 0 {
                    cvProfile.setContentOffset(CGPoint(x: width * CGFloat(currentIndex - 1), y:0), animated: true)
                }
            }
            
        }
    }
    
    @IBAction func rightAction(_ sender: Any) {
        
        if cvProfile.contentOffset.x < CGFloat(Int(cvProfile.frame.size.width) * (_count-1)) {
            let width = cvProfile.frame.size.width
            if cvProfile.indexPathsForVisibleItems.count > 0 {
                let currentIndex = cvProfile.indexPathsForVisibleItems[0].row
                if currentIndex < _count - 1 {
                    cvProfile.setContentOffset(CGPoint(x: width * CGFloat(currentIndex + 1), y:0), animated: true)
                }
            }
            
            
            
            print(cvProfile.frame.size.width)

        }
    }
    
}

extension InviteDecideViewController : UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout:UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: CGFloat(w), height: CGFloat(h))
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
