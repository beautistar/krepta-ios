//
//  InviteAcceptViewController.swift
//  Krepta
//
//  Created by Beautistar on 14/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

/////////////
//
//
//  It is for save condition from home page
//

import UIKit
import SwiftyUserDefaults

class InviteAcceptViewController: BaseViewController, UITextFieldDelegate {

    @IBOutlet weak var tfCondition: UITextField!
    var _from:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func saveAction(_ sender: Any) {
        
        if !(tfCondition.text?.isEmpty)! && (tfCondition.text?.length)! != 0 {
        
            isReply = true
            AppDelegate.getUser()._condition = tfCondition.text!
            Defaults[.condition] = tfCondition.text!
            
            if _from == Constant.FROM_HOME {
                
                self.navigationController?.popViewController(animated: true)
            } else {
                let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
            }
        } else {
            self.showAlertDialog(title: nil, message: "Please input your condition less than 90 letters", positive: R.string.OK, negative: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func chatAction(_ sender: Any) {
        
        let chatListVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatListViewController") as! ChatListViewController
        self.navigationController?.pushViewController(chatListVC, animated: true)
    }
    
    //MAEK: - TextField return key delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.characters.count - range.length
        return newLength <= 90
    }
}
