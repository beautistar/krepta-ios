

import UIKit
import Alamofire
import SwiftyJSON

class PhotoViewController: BaseViewController {
    
    var sendButton:UIButton!
    
    var _target_id = 0
    var _imgPath = ""
    var _from = 0

	override var prefersStatusBarHidden: Bool {
		return true
	}

	private var backgroundImage: UIImage

	init(image: UIImage) {
        
        self.backgroundImage = image
        
        super.init(nibName: nil, bundle: nil)
        
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	override func viewDidLoad() {
        
		super.viewDidLoad()
        
        _imgPath = saveToFile(image: backgroundImage, filePath: Constant.SAVE_ROOT_PATH, fileName: "image1.png")
        
        self.view.backgroundColor = UIColor.gray
        let backgroundImageView = UIImageView(frame: view.frame)
        backgroundImageView.contentMode = UIViewContentMode.scaleAspectFit
        backgroundImageView.image = backgroundImage
        view.addSubview(backgroundImageView)
        
        self.navigationController?.navigationBar.isHidden = true		
	}
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let cancelButton = UIButton(frame: CGRect(x: 10.0, y: 20.0, width: 30.0, height: 30.0))
        cancelButton.setImage(#imageLiteral(resourceName: "ic_cancel"), for: UIControlState())
        cancelButton.addTarget(self, action: #selector(cancel), for: .touchUpInside)
        view.addSubview(cancelButton)
        
        // add custom components
        
        if _from != Constant.FROM_CHATTING {
            
            //create label
            let wellLabel = UILabel(frame: CGRect(x:0, y: 0, width : 300, height: 30))
            wellLabel.text = "Well Done!!"
            wellLabel.textColor = UIColor.white
            wellLabel.textAlignment = NSTextAlignment.center
            wellLabel.font = UIFont(name:"HelveticaNeue", size: 25)
            wellLabel.backgroundColor = UIColor.clear
            wellLabel.center = CGPoint(x: view.center.x, y: 115)
            view.addSubview(wellLabel)
            
            let dareLabel = UILabel(frame: CGRect(x:0, y: 0, width : 300, height: 30))
            dareLabel.text = "Dare Recorded"
            dareLabel.textColor = UIColor.white
            dareLabel.textAlignment = NSTextAlignment.center
            dareLabel.font = UIFont(name:"HelveticaNeue", size: 25)
            dareLabel.backgroundColor = UIColor.clear
            dareLabel.center = CGPoint(x: view.center.x, y: 145)
            view.addSubview(dareLabel)
            
            sendButton = UIButton(frame: CGRect(x: 0, y: 0, width: 150, height: 50.0))
            sendButton.addTarget(self, action: #selector(sendAction(_:)), for: .touchUpInside)
            sendButton.setTitle("Send privately", for: .normal)
            sendButton.setTitleColor(UIColor.white, for: .normal)
            sendButton.backgroundColor = Constant.mainColor
            sendButton.layer.cornerRadius = 5.0
            sendButton.layer.masksToBounds = true
            sendButton.center = CGPoint(x: view.center.x, y: view.center.y + 200)
            self.view.addSubview(sendButton)
        }
    }
    
    @objc fileprivate func sendAction(_ sender: Any) {
        
        showLoadingView()
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in

                multipartFormData.append("\(AppDelegate.getUser()._id)".data(using:String.Encoding.utf8)!, withName: "user_id")
                multipartFormData.append("\(self._target_id)".data(using:String.Encoding.utf8)!, withName: "sender_id")
                multipartFormData.append("\(1)".data(using:String.Encoding.utf8)!, withName: "type")
                multipartFormData.append(URL(fileURLWithPath: self._imgPath), withName: "file")
                
        },
            to: Constant.REQ_SENDTOREADYPLAY,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print("register response : ", response)
                        
                        if let result = response.result.value {
                            
                            self.hideLoadingView()
                            
                            let dict = JSON(result)
                            
                            let result_code = dict[Constant.RES_RESULTCODE].intValue
                            
                            if result_code == Constant.CODE_SUCCESS {
                                
                                self.showToast("Successfully sent")
                                
                                //TODO SUCCESS :
                                if let viewControllers = self.navigationController?.viewControllers {
                                
                                //let viewControllers = self.navigationController!.viewControllers
                                
                                    if self._from == Constant.FROM_READYPLAYLIST  || self._from == Constant.FROM_INVITES {
                                        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 4], animated: true)
                                        
                                    } else {
                                        
                                        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
                                    }
                                }
                            } else {
                                
                                self.hideLoadingView()
                                self.showAlertDialog(title: R.string.ERROR, message: R.string.UPLOAD_FAIL, positive: R.string.OK, negative: nil)
                            }
                            
                        } else  {
                            
                            self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.hideLoadingView()
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)                    
                    return
                }
        }
        )
    }

	func cancel() {

        self.navigationController?.popViewController(animated: true)
	}
}
