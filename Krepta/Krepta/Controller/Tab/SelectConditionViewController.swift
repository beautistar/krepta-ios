//
//  SelectConditionViewController.swift
//  Krepta
//
//  Created by Beautistar on 11/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//
//
//
//
//  This is for send Reply to invite user
//
//
//


import UIKit
import Alamofire
import SwiftyJSON

class SelectConditionViewController: BaseViewController, UITextFieldDelegate {

    @IBOutlet weak var tfCondition: UITextField!
    
    var _senderUser = UserEntity()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func sendAction(_ sender: Any) {
            
        let reply_condition = tfCondition.text!
            
        Alamofire.upload(
            multipartFormData: { multipartFormData in

                multipartFormData.append("\(AppDelegate.getUser()._id)".data(using:String.Encoding.utf8)!, withName: "user_id")
                multipartFormData.append("\(self._senderUser._id)".data(using:String.Encoding.utf8)!, withName: "sender_id")
                multipartFormData.append("\(reply_condition)".data(using:String.Encoding.utf8)!, withName: "condition")
                
        },
            to: Constant.REQ_REPLYINVITE,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print("reply invite response : ", response)
                        
                        if let result = response.result.value {
                            
                            let dict = JSON(result)
                            
                            let result_code = dict[Constant.RES_RESULTCODE].intValue
                            
                            if result_code == Constant.CODE_SUCCESS {
                                
                                //TODO : success
                                
                                self.perform(#selector(self.goBackToInvite), with: nil, afterDelay: 0.1)
                            }
                            
                        } else  {
                            
                            //self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    
                    self.hideLoadingView()
                    
                    return
                }
        }
        )
    }
    
    func goBackToInvite() {
        
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
    }
    
    //MAEK: - TextField return key delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.characters.count - range.length
        return newLength <= 90
    }
}
