//
//  RecordViewController.swift
//  Krepta
//
//  Created by Beautistar on 16/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import SwiftyCam
import Toaster
import KDCircularProgress
import Alamofire
import SwiftyJSON

class RecordViewController: SwiftyCamViewController, SwiftyCamViewControllerDelegate {

    var flashButton:UIButton!
    var progress: KDCircularProgress!
    var _sender_id = 0
    var _from = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        cameraDelegate = self
        maximumVideoDuration = 60.0
        shouldUseDeviceOrientation = true
//        allowAutoRotate = true
//        audioEnabled = true
        allowBackgroundAudio = true        
        
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        
        self.navigationController?.navigationBar.isHidden = true
        
        //let cameraButton = SwiftyCamButton(frame: CGRect(x: view.frame.midX - 100, y: view.frame.height - 100.0, width: 50.0, height: 50.0))
        let cameraButton = UIButton(frame: CGRect(x: view.frame.midX - 100, y: view.frame.height - 150.0, width: 50.0, height: 50.0))
        cameraButton.setImage(#imageLiteral(resourceName: "ic_rec_camera"), for: UIControlState())
        cameraButton.addTarget(self, action: #selector(takePhotoClick(_:)), for: .touchUpInside)
        self.view.addSubview(cameraButton)
        //        cameraButton.delegate = self
        
        let videoButton = UIButton(frame: CGRect(x: view.frame.midX + 50, y: view.frame.height - 150.0, width: 50.0, height: 50.0))
        videoButton.setImage(#imageLiteral(resourceName: "ic_rec_video"), for: UIControlState())
        videoButton.addTarget(self, action: #selector(recordVideoClick(_:)), for: .touchUpInside)
        self.view.addSubview(videoButton)
        //videoButton.delegate = self
        
        let flipCameraButton = UIButton(frame: CGRect(x: (view.frame.width  - 50.0), y: 25.0, width: 40.0, height:35.0))
        flipCameraButton.setImage(#imageLiteral(resourceName: "ic_camera_reverse"), for: UIControlState())
        flipCameraButton.addTarget(self, action: #selector(cameraSwitchAction(_:)), for: .touchUpInside)
        self.view.addSubview(flipCameraButton)
        
        //let test = CGFloat((view.frame.width - (view.frame.width / 2 + 37.5)) + ((view.frame.width / 2) - 37.5) - 9.0)
        let backButton = UIButton(frame: CGRect(x: 10, y: 25, width: 40, height: 40.0))
        backButton.setImage(#imageLiteral(resourceName: "ic_back_blue"), for: UIControlState())
        backButton.addTarget(self, action: #selector(backAction(_:)), for: .touchUpInside)
        self.view.addSubview(backButton)
        
        flashButton = UIButton(frame: CGRect(x: 55, y: 25, width: 40.0, height: 40.0))
        flashButton.setImage(#imageLiteral(resourceName: "ic_flash"), for: UIControlState())
        flashButton.addTarget(self, action: #selector(toggleFlashAction(_:)), for: .touchUpInside)
        self.view.addSubview(flashButton)
        
        let titleLbl = UILabel(frame: CGRect(x:view.frame.midX-50, y: 30, width : 150, height: 25))
        titleLbl.text = "Krepta"
        titleLbl.font = UIFont(name: "Space Age", size: 25)
        titleLbl.textColor = UIColor.white
        titleLbl.textAlignment = NSTextAlignment.center
        titleLbl.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.1)
        
        self.view.addSubview(titleLbl)
        
        // add no need button
        let noNeedBtn = UIButton.init(frame: CGRect(x: view.frame.midX - 50, y: view.frame.height - 80.0, width: 100.0, height: 50.0))
        noNeedBtn.backgroundColor = Constant.mainColor
        noNeedBtn.setTitle("No need", for: .normal)
        noNeedBtn.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        noNeedBtn.layer.cornerRadius = 5.0
        noNeedBtn.layer.masksToBounds = true
        noNeedBtn.addTarget(self, action: #selector(noNeedTapped(_:)), for: .touchUpInside)
        self.view.addSubview(noNeedBtn)
        
        // add video record progress bar
        progress = KDCircularProgress(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
        progress.startAngle = -90
        progress.progressThickness = 0.2
        progress.trackThickness = 0.6
        progress.clockwise = true
        progress.gradientRotateSpeed = 1
        progress.roundedCorners = false
        progress.glowMode = .forward
        progress.glowAmount = 0.1
        progress.trackColor = Constant.mainColor
        progress.set(colors: UIColor.red, UIColor.red, UIColor.red)
        progress.center = CGPoint(x: view.center.x, y: view.center.y + 100)
        view.addSubview(progress)
        
        progress.isHidden = true
        
        if _from == Constant.FROM_RATE {
            noNeedBtn.isHidden = true
        } else {
            noNeedBtn.isHidden = false
        }

        print("from ==", _from)
    }
    
    @objc private func cameraSwitchAction(_ sender: Any) {
        switchCamera()
    }
    
    @objc private func noNeedTapped(_ sender: Any) {

        goBack()
    }
    
    @objc private func toggleFlashAction(_ sender: Any) {
        flashEnabled = !flashEnabled
        
        if flashEnabled == true {
            flashButton.setImage(#imageLiteral(resourceName: "ic_flash"), for: UIControlState())
        } else {
            flashButton.setImage(#imageLiteral(resourceName: "ic_flash_off"), for: UIControlState())
        }
    }
    
    @objc private func takePhotoClick(_ sender: Any) {
        
        takePhoto()
    }
    
    @objc private func recordVideoClick(_ sender: Any) {
        
        if isVideoRecording {
            stopVideoRecording()
        } else {
            startVideoRecording()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @objc private func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - API
    
    func goBack() {
        
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
    }
 
    // MARK: - SwiftyCam delegate
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didTake photo: UIImage) {
        // Called when takePhoto() is called or if a SwiftyCamButton initiates a tap gesture
        // Returns a UIImage captured from the current session
        
        let newVC = PhotoViewController(image: photo)
        newVC._target_id = _sender_id
        newVC._from = self._from

        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didBeginRecordingVideo camera: SwiftyCamViewController.CameraSelection) {
        // Called when startVideoRecording() is called
        // Called if a SwiftyCamButton begins a long press gesture
        print("did begin record video")
        
        progress.isHidden = false
        progress.animate(fromAngle: 0, toAngle: 360, duration: 60) { completed in
            if completed {
                print("animation stopped, completed")
                self.stopVideoRecording()
            } else {
                print("animation stopped, was interrupted")
            }
        }
        Toast.init(text: "Start recording...", delay: 0, duration: 0.5).show()
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishRecordingVideo camera: SwiftyCamViewController.CameraSelection) {
        // Called when stopVideoRecording() is called
        // Called if a SwiftyCamButton ends a long press gesture
        print("did finish record video")
        Toast.init(text: "Stop recording", delay: 0, duration: 0.5).show()
        progress.isHidden = true
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishProcessVideoAt url: URL) {
        // Called when stopVideoRecording() is called and the video is finished processing
        // Returns a URL in the temporary directory where video is stored
        
        let newVC = VideoViewController(videoURL: url)
        newVC._target_id = _sender_id
        newVC._from = self._from
        //self.present(newVC, animated: true, completion: nil)
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFocusAtPoint point: CGPoint) {
        // Called when a user initiates a tap gesture on the preview layer
        // Will only be called if tapToFocus = true
        // Returns a CGPoint of the tap location on the preview layer
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didChangeZoomLevel zoom: CGFloat) {
        // Called when a user initiates a pinch gesture on the preview layer
        // Will only be called if pinchToZoomn = true
        // Returns a CGFloat of the current zoom level
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didSwitchCameras camera: SwiftyCamViewController.CameraSelection) {
        // Called when user switches between cameras
        // Returns current camera selection   
    }
}

extension RecordViewController {
    
    func showLoadingView() {
        
        showLoadingViewWithTitle(title: "")
    }
    
    
    func showLoadingViewWithTitle(title: String) {
        
        if title == "" {
            
            ProgressHUD.show()
            
        } else {
            
            ProgressHUD.showWithStatus(string: title)
        }
    }
    
    // hide loading view
    func hideLoadingView() {
        
        ProgressHUD.dismiss()
    }
    
    //Toast
    func showToast(_ text:String) {
        
        Toast(text:text).show()
    }
    
    func showAlertDialog(title: String!, message: String!, positive: String?, negative: String?) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if (positive != nil) {
            
            alert.addAction(UIAlertAction(title: positive, style: .default, handler: nil))
        }
        
        if (negative != nil) {
            
            alert.addAction(UIAlertAction(title: negative, style: .default, handler: nil))
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
}
