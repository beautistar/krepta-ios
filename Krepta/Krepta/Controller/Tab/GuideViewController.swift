//
//  GuideViewController.swift
//  Krepta
//
//  Created by Beautistar on 27/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class GuideViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var cvGuide: UICollectionView!
    
    var w:CGFloat = 0.0
    var h:CGFloat = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        w = self.view.frame.size.width * 0.9
        h = self.view.frame.size.height * 0.75
        
    }
    //MARK: - CollectionView

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GuideCollectionCell", for: indexPath) as! GuideCollectionCell
        
        switch indexPath.row {
        case 0:
            cell.imvLeft.isHidden = true
            cell.imvRight.image = #imageLiteral(resourceName: "intro_step1")
            cell.lblStep.text = "Step 1"
            cell.lblGuide.text = "Before you Begin Use the Rocket Icon on the top right to tell other users what you want them to do."
            break
        case 1:
            cell.imvLeft.image = #imageLiteral(resourceName: "intro_step2_1")
            cell.imvRight.image = #imageLiteral(resourceName: "intro_step2_2")
            cell.lblStep.text = "Step 2"
            cell.lblGuide.text = "Swipe Right on images to Invite users to perform the condition you selected or Left to remove them from your Feed!"
            break
        default:
            cell.imvLeft.image = #imageLiteral(resourceName: "intro_step3")
            cell.imvRight.image = #imageLiteral(resourceName: "intro_step3_1")
            cell.lblStep.text = "Step 3"
            cell.lblGuide.text = "Use Scroll bar on the right side of the screen to scroll."
            break
        }
        
        return cell
    }
    
    @IBAction func rightAction(_ sender: Any) {
        
        if cvGuide.contentOffset.x < CGFloat(Int(cvGuide.frame.size.width) * (3-1)) {
            let width = w
            if cvGuide.indexPathsForVisibleItems.count > 0 {
                let currentIndex = cvGuide.indexPathsForVisibleItems[0].row
                if currentIndex < 3 - 1 {
                    cvGuide.setContentOffset(CGPoint(x: width * CGFloat(currentIndex + 1), y:0), animated: true)

                }
            }
            
            print(cvGuide.frame.size.width)

        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension GuideViewController:UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout:UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: CGFloat(w), height: CGFloat(h))
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}
