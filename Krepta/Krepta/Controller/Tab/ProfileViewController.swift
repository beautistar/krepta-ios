//
//  ProfileViewController.swift
//  Krepta
//
//  Created by Beautistar on 6/9/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import ActionSheetPicker
import HGCircularSlider
import SwiftyUserDefaults
import Alamofire
import SwiftyJSON


class ProfileViewController: BaseViewController, UITextFieldDelegate {

    @IBOutlet weak var btnSelection: UIButton!
    @IBOutlet weak var tfSelection: UITextField!
    @IBOutlet weak var distanceRangeSlider: CircularSlider!
    @IBOutlet weak var ageCircularSlider: RangeCircularSlider!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var imvPhoto: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var tfStatus: UITextField!
    
    var selectedStartAge:Int = 14
    var selectedEndAge:Int = 100
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        
        updateAgeTexts()
        distanceRangeSlider.addTarget(self, action: #selector(updateAgeTexts), for: .valueChanged)
        
        // Age change
        
        ageCircularSlider.maximumValue = 100
        ageCircularSlider.minimumValue = 1.0
        
        ageCircularSlider.startPointValue = 14
        ageCircularSlider.endPointValue = 100
        
        setAgeText(ageCircularSlider)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)        
        
        // set user
        lblName.text = AppDelegate.getUser()._name
        imvPhoto.sd_setImage(with: URL(string: AppDelegate.getUser()._profileUrls[0]), placeholderImage: #imageLiteral(resourceName: "img_user"))
        
    }
    
    func updateAgeTexts() {
        
        /// Distance Change
        
        let value = distanceRangeSlider.endPointValue
        lblDistance.text = String(format: "%.0f", value) + " Km"
        
        /// setup filter distance
        _distance = Int(value)
    }
    
    @IBAction func setAgeText(_ sender: Any) {
        
        if ageCircularSlider.startPointValue < 14  {
            ageCircularSlider.startPointValue = 14
            ageCircularSlider.endEditing(true)
        }
        
        if ageCircularSlider.endPointValue < ageCircularSlider.startPointValue {
            ageCircularSlider.endPointValue = ageCircularSlider.startPointValue + 1
            ageCircularSlider.endEditing(true)
        }
        
        selectedStartAge = Int(ageCircularSlider.startPointValue)
        selectedEndAge = Int(ageCircularSlider.endPointValue)
        
        _startAge = selectedStartAge
        _endAge = selectedEndAge
        
        lblAge.text = String(format: "%d-%d", _startAge , _endAge)
    }
   
    
    @IBAction func editProfileAction(_ sender: Any) {
        
        let editProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfilePhotoViewController") as! ProfilePhotoViewController
        editProfileVC._imgUrl1 = ""
        editProfileVC._user_status = self.tfStatus.text!
        editProfileVC._from = Constant.FROM_PROFILE
        self.navigationController?.pushViewController(editProfileVC, animated: true)       
    }

    @IBAction func selectFeedAction(_ sender: Any) {
        
        ActionSheetStringPicker.show(withTitle: "Selection", rows: ["Anyone", "Males", "Females"], initialSelection: 0, doneBlock: {
            picker, value, index in
            
            print("value = \(value)")
            print("index = \(String(describing: index))")
            print("picker = \(String(describing: picker))")
            
            switch index as! String {
            case "Anyone" :
                _gender = "Anyone"
                break
            case "Females" :
                _gender = "female"
                break
            default :
                _gender = "male"
                break
            }
            
            self.tfSelection.text = index as? String
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    //MAEK: - TextField return key delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        updateStatus()
        
        self.view.endEditing(true)
        return true
    }
    
    /// Update user Status API
    
    func updateStatus() {
        
        showLoadingView()
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                multipartFormData.append("\(AppDelegate.getUser()._id)".data(using:String.Encoding.utf8)!, withName: "id")
                multipartFormData.append("\(self.tfStatus.text!)".data(using:String.Encoding.utf8)!, withName: "status")
        },
            to: Constant.REQ_UPDATESTATUS,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print("update status response : ", response)
                        
                        if let result = response.result.value {
                            
                            self.hideLoadingView()
                            
                            let dict = JSON(result)
                            
                            let result_code = dict[Constant.RES_RESULTCODE].intValue
                            
                            if result_code == Constant.CODE_SUCCESS {
                                
                                AppDelegate.getUser()._status = self.tfStatus.text!
                                Defaults[.status] = self.tfStatus.text!
                            }
                            
                        } else  {
                            
                            self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.hideLoadingView()
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    return
                    
                }
        }
        )        
    }
}
