//
//  InviteViewController.swift
//  Krepta
//
//  Created by Beautistar on 6/9/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class InviteViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tblUserList: UITableView!
    
    var _invites : [InviteEntity] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tblUserList.tableFooterView = UIView()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = false
        
        if isReply {
            self.tabBarController?.selectedIndex = 0
        } else {
            self.tabBarController?.selectedIndex = 1
        }
        
        isReply = false
        
        // if this view opened back from inview detail view, don't call this API
        if !_inviteViewed {
            getInvite()
        }
        _inviteViewed = false
    }
    
    func getInvite() {
        
        self._invites.removeAll()
        self.tblUserList.reloadData()
        
        showLoadingView()
        
        print(AppDelegate.getUser()._id)        
     
        let URL = Constant.REQ_GETINVITE + "\(AppDelegate.getUser()._id)/"
        print(URL)
        
        Alamofire.request(URL, method:.get)
            .responseJSON { response in
                
                if response.result.isFailure {
                    
                    self.hideLoadingView()
                    self.showToast(R.string.CONNECT_FAIL)
                    return
                }
                
                if let result = response.result.value  {
                    
                    self.hideLoadingView()
                    
                    print(result)
                    
                    let dict = JSON(result)
                    
                    print(dict)
                    
                    let result_code = dict[Constant.RES_RESULTCODE].intValue
                    
                    if result_code == Constant.CODE_SUCCESS {
                        
                        let inviteInfos = dict[Constant.RES_RECEIVED_INVITE].arrayValue
                        
                        for i in 0..<inviteInfos.count {
                            
                            let _invite = InviteEntity()
                            
                            _invite._inviteUser._id = inviteInfos[i]["invite_user"]["id"].intValue
                            _invite._inviteUser._username = inviteInfos[i]["invite_user"]["username"].stringValue
                            _invite._inviteUser._name = inviteInfos[i]["invite_user"]["name"].stringValue
                            _invite._inviteUser._email = inviteInfos[i]["invite_user"]["email"].stringValue
                            _invite._inviteUser._age = inviteInfos[i]["invite_user"]["age"].intValue
                            _invite._inviteUser._gender = inviteInfos[i]["invite_user"]["gender"].stringValue
                            _invite._inviteUser._status = inviteInfos[i]["invite_user"]["status"].stringValue
                            
                            let photo_urls = inviteInfos[i]["invite_user"]["photo_url"].arrayValue
                            _invite._inviteUser._profileUrls.removeAll()
                            for j in 0..<photo_urls.count {
                                
                                _invite._inviteUser._profileUrls.append(photo_urls[j].stringValue)
                            }
                            
                            _invite._invite_condition = inviteInfos[i]["invite_condition"].stringValue
                            _invite._reply_condition = inviteInfos[i]["reply_condition"].stringValue
                             _invite._request_userid = inviteInfos[i]["request_user"].intValue
                            _invite._status = inviteInfos[i]["status"].intValue
                            
                            self._invites.append(_invite)
                        }
                        
                        self.tblUserList.reloadData()
                    }
                }
        }
    }
    
    // MARK: - TableView delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return _invites.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 76
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendCell") as! FriendCell
        
        if _invites[indexPath.row]._status == 0 {
            cell.lblHiden.isHidden = false
        } else {
            cell.lblHiden.isHidden = true
        }
        
        cell.lblUserName.text = _invites[indexPath.row]._inviteUser._name
        cell.imvUserPhoto.sd_setImage(with: URL(string:_invites[indexPath.row]._inviteUser._profileUrls[0]), placeholderImage: #imageLiteral(resourceName: "img_user"))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if _invites[indexPath.row]._status != 0 {
            
            let conditionVC = self.storyboard?.instantiateViewController(withIdentifier: "UserConditionsViewController") as! UserConditionsViewController
            conditionVC._from = Constant.FROM_INVITES
            conditionVC._selectedInvite = self._invites[indexPath.row]
            self.navigationController?.pushViewController(conditionVC, animated: true)
        
        } else {
            
            let inviteDecideVC = self.storyboard?.instantiateViewController(withIdentifier: "InviteDecideViewController") as! InviteDecideViewController// user profile view
            inviteDecideVC._from = Constant.FROM_INVITES
            inviteDecideVC._selectedUser = self._invites[indexPath.row]._inviteUser
            inviteDecideVC._challenge = self._invites[indexPath.row]._invite_condition
            self.navigationController?.pushViewController(inviteDecideVC, animated: true)
        }
    }
}
