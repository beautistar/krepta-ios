//
//  UserConditionsViewController.swift
//  Krepta
//
//  Created by Beautistar on 15/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class UserConditionsViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    var _from:Int = 0
    @IBOutlet weak var btnStart: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet weak var btnYes: UIButton!

    var _selectedInvite = InviteEntity()

    @IBOutlet weak var tblConditions: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initView()
    }
    
    func initView() {
        
        if _from == Constant.FROM_READYPLAYLIST {
            
            isReply = false
            btnStart.isHidden = false
            btnNo.isHidden = true
            btnYes.isHidden = true
        } else {
            
            isReply = true
            btnStart.isHidden = true
            btnNo.isHidden = false
            btnYes.isHidden = false
        }
        
        self.navigationController?.navigationBar.barTintColor = Constant.mainColor
        self.navigationController?.navigationBar.isHidden = false
        
        tblConditions.tableFooterView = UIView()
        tblConditions.estimatedRowHeight = 70
        tblConditions.rowHeight = UITableViewAutomaticDimension
        
    }
    
    @IBAction func noAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func yesAction(_ sender: Any) {
        
        self.confirmInvite()
    }
    
    //MARK: - Segue for YES and START
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        //if START button clicked :
        if segue.identifier == "segueForStart" {
            
            let recordVC = segue.destination as! RecordViewController
            recordVC._sender_id = _selectedInvite._inviteUser._id
            recordVC._from = self._from
        }
    }
    
    // MARK: - confirmInvite
    
    func confirmInvite() {
        
        showLoadingView()
        
        print("accept no need", AppDelegate.getUser()._id, self._selectedInvite._inviteUser._id)
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                multipartFormData.append("\(AppDelegate.getUser()._id)".data(using:String.Encoding.utf8)!, withName: "user_id")
                multipartFormData.append("\(self._selectedInvite._inviteUser._id)".data(using:String.Encoding.utf8)!, withName: "sender_id")
        },
            to: Constant.REQ_CONFIRMRINVITE,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print("confirm invite : ", response)
                        
                        self.hideLoadingView()
                        
                        if let result = response.result.value {
                            
                            let dict = JSON(result)
                            
                            let result_code = dict[Constant.RES_RESULTCODE].intValue
                            
                            if result_code == Constant.CODE_SUCCESS {
                                
                                //TODO : success
                                self.perform(#selector(self.gotoRecordVC), with: nil, afterDelay: 0.5)
                            }
                            
                        } else  {
                            
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    
                    self.hideLoadingView()
                    
                    return
                }
        })
    }
    
    func gotoRecordVC() {
        
        let recordVC = self.storyboard?.instantiateViewController(withIdentifier: "RecordViewController") as! RecordViewController
        recordVC._sender_id = _selectedInvite._inviteUser._id
        recordVC._from = _from
        self.navigationController?.pushViewController(recordVC, animated: true)
    }
    
    
    // MARK: - TableView Delegate Datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row % 2 == 0 {
            return 70
        } else {
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row % 2 == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CondtionUserCell") as! CondtionUserCell
            if indexPath.row == 0 {
                
                if _selectedInvite._request_userid == AppDelegate.getUser()._id {
                    
                    cell.imvPhoto.sd_setImage(with: URL(string: AppDelegate.getUser()._profileUrls[0]), placeholderImage: #imageLiteral(resourceName: "img_user"))
                    cell.lblUserName.text = AppDelegate.getUser()._name
                    
                } else {
                
                    cell.imvPhoto.sd_setImage(with: URL(string: _selectedInvite._inviteUser._profileUrls[0]), placeholderImage: #imageLiteral(resourceName: "img_user"))
                    cell.lblUserName.text = _selectedInvite._inviteUser._name
                }
                
            } else {
                
                if _selectedInvite._request_userid != AppDelegate.getUser()._id {
                    
                    cell.imvPhoto.sd_setImage(with: URL(string: AppDelegate.getUser()._profileUrls[0]), placeholderImage: #imageLiteral(resourceName: "img_user"))
                    cell.lblUserName.text = AppDelegate.getUser()._name
                    
                } else {
                    
                    cell.imvPhoto.sd_setImage(with: URL(string: _selectedInvite._inviteUser._profileUrls[0]), placeholderImage: #imageLiteral(resourceName: "img_user"))
                    cell.lblUserName.text = _selectedInvite._inviteUser._name
                }
            }
            
            return cell
            
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConditionTextCell") as! ConditionTextCell
            if indexPath.row == 1 {
                cell.lblCondition.text = String(format: "I want you to.. %@", self._selectedInvite._invite_condition)
            } else {
                cell.lblCondition.text = String(format: "I want you to.. %@", self._selectedInvite._reply_condition)
            }
            
            return cell
        }
    }
}
