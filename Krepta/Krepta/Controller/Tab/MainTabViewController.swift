//
//  MainTabViewController.swift
//  Krepta
//
//  Created by Beautistar on 6/9/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class MainTabViewController: UITabBarController, UITabBarControllerDelegate {
    
    var btnChat:UIBarButtonItem!
    var btnRocket:UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setNavigationBarItem()
        
        initTabbar()
        
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Space Age", size: 23)!, NSForegroundColorAttributeName: UIColor.white]
        
        self.navigationItem.title = "Krepta"
        
        btnChat = UIBarButtonItem(image: UIImage(named: "ic_chats"), style:.plain,target: self, action: #selector(MainTabViewController.gotoChat))
        btnChat.image = btnChat.image?.withRenderingMode(.alwaysOriginal)
        
        btnRocket = UIBarButtonItem(image:UIImage(named:"ic_rocket_white"), style:.plain, target:self, action:#selector(MainTabViewController.gotoWant))
        btnChat.image = btnChat.image?.withRenderingMode(.alwaysOriginal)
        
            self.navigationItem.rightBarButtonItems  = [btnRocket, btnChat]
    }
    
    func gotoChat() {
        
        let chatListVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatListViewController") as! ChatListViewController
        self.navigationController?.pushViewController(chatListVC, animated: true)
    }
    
    func gotoWant() {
        
        let selectConditionVC = self.storyboard?.instantiateViewController(withIdentifier: "InviteAcceptViewController") as! InviteAcceptViewController
        selectConditionVC._from = Constant.FROM_HOME
        self.navigationController?.pushViewController(selectConditionVC, animated: true)
    }
    
    
    func initTabbar() {
        
        self.tabBarController?.delegate = self
        
        
        if let items = self.tabBar.items
        {
            for item in items
            {
                if let image = item.image
                {
                    item.image = image.withRenderingMode( .alwaysOriginal )
                    item.selectedImage = image.withRenderingMode(.alwaysOriginal)
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func gotoSetting() {
        
        print("goto setting")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }

    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        self.navigationItem.rightBarButtonItems = nil
        if item == (self.tabBar.items!)[1] {
            self.navigationItem.rightBarButtonItems  = [btnRocket, btnChat]
        }
        else {
            self.navigationItem.rightBarButtonItem = btnChat
        }
    }
}
