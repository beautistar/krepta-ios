//
//  HomeViewController.swift
//  Krepta
//
//  Created by Beautistar on 6/9/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import Foundation
import Toaster
import Presentr
import Alamofire
import SwiftyJSON
import SwiftPullToRefresh
import SwiftyUserDefaults

class HomeViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    let presenter: Presentr = {
        let presenter = Presentr(presentationType: .alert)
        presenter.transitionType = TransitionType.coverHorizontalFromRight
        presenter.dismissOnSwipe = true
        return presenter
    }()
    
    var g_user:UserEntity!

    @IBOutlet weak var tblHome: UITableView!
    var storedOffsets = [Int: CGFloat]()
    var page_index:Int = 1
    var curPageIndex:Int = 0
    var curGender:String = "Anyone"
    var curAgeStart:Int = 0
    var curAgeEnd : Int = 100
    var curDistance : Int = 0
    
    var totalUsers : Int = 0
    var swipedUsers : Int = 0
    
    var _userInfos = [UserEntity]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }
    
    func initView() {
        
        g_user = AppDelegate.getUser()
        
        page_index = 1
        curPageIndex = 0
        
        print(g_user._latitude, g_user._longitude)
        
        tblHome.tableFooterView = UIView()
        tblHome.estimatedRowHeight = 300
        tblHome.rowHeight = UITableViewAutomaticDimension
        
        // Create the Intro dialogs
        
        if _loggedIn {
        
            let guidePopVC = self.storyboard?.instantiateViewController(withIdentifier: "GuideViewController") as! GuideViewController
            presenter.presentationType = .custom(width: ModalSize.fluid(percentage: 0.9) , height: ModalSize.fluid(percentage: 0.75), center: ModalCenterPosition.center)
            presenter.transitionType = nil
            presenter.dismissTransitionType = nil
            presenter.dismissAnimated = true
            presenter.roundCorners = true
            presenter.cornerRadius = 10
            customPresentViewController(presenter, viewController: guidePopVC, animated: true, completion: nil)            
            
            _loggedIn = false
        }
        
        //bottom refresh
        tblHome.spr_setTextAutoFooter { [weak self] in
            self?.action()
        }
        
        getHomeUsers()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tblHome.showsVerticalScrollIndicator = true
        tblHome.flashScrollIndicators()
        
        //pull to(Auto) bottom refresh
        tblHome.spr_beginRefreshing()
        
        //check if filter option is changed or not
        
        if curGender != _gender || curAgeStart != _startAge || curAgeEnd != _endAge || curDistance != _distance {
            
            curGender = _gender
            curAgeStart = _startAge
            curAgeEnd = _endAge
            curDistance = _distance
            
            page_index = 1
            curPageIndex = 0

            getHomeUsers()
        } else {
        
            tblHome.reloadData()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)        
    }    
    
    private func action() {
        
        page_index = curPageIndex + 1
        
        getHomeUsers()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.tblHome.spr_endRefreshing()
        }
    }
    
    func gotoChat() {
        
        let chatListVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatListViewController") as! ChatListViewController
        self.navigationController?.pushViewController(chatListVC, animated: true)
    }
    
    func gotoSetCondition() {
        
        let selectConditionVC = self.storyboard?.instantiateViewController(withIdentifier: "InviteAcceptViewController") as! InviteAcceptViewController
        selectConditionVC._from = Constant.FROM_HOME
        self.navigationController?.pushViewController(selectConditionVC, animated: true)
    }
    
    // MARK: - getHomeUsers API 
    
    func getHomeUsers() {
        
        //showLoadingView()
        if page_index == 1 {
            self._userInfos.removeAll()
            tblHome.reloadData()
        }
        
        print(self.g_user._latitude, self.g_user._longitude, _gender, _distance)
        
        if g_user._latitude == 0 {
            g_user._latitude = Defaults[.latitude]
        } else {
            Defaults[.latitude] = g_user._latitude
        }
        
        if g_user._longitude == 0 {
            g_user._longitude = Defaults[.longitude]
        } else {
            Defaults[.longitude] = g_user._longitude
        }
        
        print(self.g_user._latitude, self.g_user._longitude, _gender, _distance)
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                multipartFormData.append("\(self.g_user._id)".data(using:String.Encoding.utf8)!, withName: "id")
                multipartFormData.append("\(_distance)".data(using:String.Encoding.utf8)!, withName: "distance")
                multipartFormData.append("\(_startAge)".data(using:String.Encoding.utf8)!, withName: "start_age")
                multipartFormData.append("\(_gender)".data(using:String.Encoding.utf8)!, withName: "gender")
                multipartFormData.append("\(_endAge)".data(using:String.Encoding.utf8)!, withName: "end_age")
                multipartFormData.append("\(self.g_user._latitude)".data(using:String.Encoding.utf8)!, withName: "latitude")
                multipartFormData.append("\(self.g_user._longitude)".data(using:String.Encoding.utf8)!, withName: "longitude")
                multipartFormData.append("\(self.page_index)".data(using:String.Encoding.utf8)!, withName: "page_index")
                multipartFormData.append("\(Defaults[.full_swiped])".data(using:String.Encoding.utf8)!, withName: "full_swiped")
                
        },
            to: Constant.REQ_GETHOMEUSER,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print("get home user response : ", response)
                        
                        if let result = response.result.value {
                            
                            //self.hideLoadingView()
                            
                            let dict = JSON(result)
                            
                            let result_code = dict[Constant.RES_RESULTCODE].intValue
                            
                            if result_code == Constant.CODE_SUCCESS {
                                
                                let userInfos = dict[Constant.RES_USERINFO].arrayValue

                                for i in 0..<userInfos.count {
                                    
                                    let _user = UserEntity()
                                    
                                    _user._id = userInfos[i]["id"].intValue
                                    _user._username = userInfos[i]["username"].stringValue
                                    _user._name = userInfos[i]["name"].stringValue
                                    _user._email = userInfos[i]["email"].stringValue
                                    _user._age = userInfos[i]["age"].intValue
                                    _user._gender = userInfos[i]["gender"].stringValue
                                    _user._latitude = Double(userInfos[i]["latitude"].floatValue)
                                    _user._longitude = Double(userInfos[i]["longitude"].floatValue)
                                    _user._distance = Double(userInfos[i]["distance"].intValue)
                                    _user._condition = userInfos[i]["condition"].stringValue
                                    _user._status = userInfos[i]["status"].stringValue
                                    
                                    let photo_urls = userInfos[i]["photo_url"].arrayValue
                                    _user._profileUrls.removeAll()
                                    for j in 0..<photo_urls.count {
                                        
                                        _user._profileUrls.append(photo_urls[j].stringValue)
                                    }
                                   
                                    if !self._userInfos.contains(_user) {
                                        self._userInfos.append(_user)
                                    }
                                }
                                
                                self.tblHome.reloadData()

                                self.tblHome.spr_endRefreshing()
                                
                                if userInfos.count > 0 {
                                    self.curPageIndex += 1
                                }
                                
                                if self._userInfos.count == 0 {
                                    Defaults[.full_swiped] = 1
                                    self.page_index = 1
                                    self.curPageIndex = 0
                                    self.getHomeUsers()
                                } else {
                                    Defaults[.full_swiped] = 0
                                }
                            }
                            
                        } else  {
                            
                            //self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    
                    self.hideLoadingView()
                    
                    return
                }
        }
        )
    }
    
    // MARK: - Send Invite API
    func sendInvite(_user : UserEntity) {
        
        print("sender id / username : ", _user._id, _user._username)
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                multipartFormData.append("\(self.g_user._id)".data(using:String.Encoding.utf8)!, withName: "user_id")
                multipartFormData.append("\(_user._id)".data(using:String.Encoding.utf8)!, withName: "sender_id")
                multipartFormData.append("\(self.g_user._condition)".data(using:String.Encoding.utf8)!, withName: "condition")
                
        },
            to: Constant.REQ_SENDINVITE,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print("send invite response : ", response)
                        
                        if let result = response.result.value {
                            
                            let dict = JSON(result)
                            
                            let result_code = dict[Constant.RES_RESULTCODE].intValue
                            
                            if result_code == Constant.CODE_SUCCESS {
                                
                                //TODO : success
                            } else {
                                //TODO : fail                                
                            }
                            
                        } else  {
                            
                            //self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    
                    self.hideLoadingView()
                    
                    return
                }
        }
        )
    }
    
    // MARK: - tableView delegate datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //return tempDatas.count
        return _userInfos.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell") as! HomeCell

        cell.delegate = self

        cell.setUserInfo(_userInfos[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let inviteDecideVC = self.storyboard?.instantiateViewController(withIdentifier: "InviteDecideViewController") as! InviteDecideViewController
        inviteDecideVC._from = Constant.FROM_HOME
        inviteDecideVC._selectedUser = _userInfos[indexPath.row]
        self.navigationController?.pushViewController(inviteDecideVC, animated: true)
    }
}

extension HomeViewController: HomeCellDelegate {
    
    func homeCellSwiped(_ cell: HomeCell, isToRight: Bool) {

        if g_user._condition.isEmpty && g_user._condition.length == 0 {
            
            // Create the alert controller
            let alertController = UIAlertController(title: "Save your condition", message: "Save your condition to invite user.", preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                UIAlertAction in
                
                self.perform(#selector(self.gotoSetCondition), with: nil, afterDelay: 0.1)
            }
            
            // Add the actions
            alertController.addAction(okAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            return
        }
        
        if let indexPath = tblHome.indexPath(for: cell) {
            
            if isToRight {
                
                // TODO: Call send Invite API on here
                
                self.sendInvite(_user: _userInfos[indexPath.row])
                
            } else {
                // TODO: Call API on here
                
            }
            
            tblHome.beginUpdates()
            tblHome.deleteRows(at: [indexPath], with: .automatic)
            
            // tempDatas.remove(at: indexPath.row)
            _userInfos.remove(at: indexPath.row)
            tblHome.endUpdates()
            
            if _userInfos.count == 0 {
                self.getHomeUsers()
            }
            
        }
    }
}

