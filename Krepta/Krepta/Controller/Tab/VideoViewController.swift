import UIKit
import AVFoundation
import AVKit
import Alamofire
import SwiftyJSON

class VideoViewController: BaseViewController {
    
    var sendButton:UIButton!
    
    var _target_id = 0
    var _videoUrl = ""
    var _from = 0
   
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    private var videoURL: URL
    var compressedURL : URL
    var player: AVPlayer?
    var playerController : AVPlayerViewController?
    
    init(videoURL: URL) {
        self.videoURL = videoURL
        self._videoUrl = videoURL.path
        self.compressedURL = videoURL
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }    

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.gray
        self.navigationController?.navigationBar.isHidden = true
        
        player = AVPlayer(url: videoURL)
        playerController = AVPlayerViewController()
        
        guard player != nil && playerController != nil else {
            return
        }
        
        if _from == Constant.FROM_CHATTING {
            playerController!.showsPlaybackControls = true
        } else {
            playerController!.showsPlaybackControls = false
        }
        
        playerController!.player = player!
        self.addChildViewController(playerController!)
        self.view.addSubview(playerController!.view)
        playerController!.view.frame = view.frame
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.player!.currentItem)
        
        compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".m4v")
        compressVideo(inputURL: videoURL , outputURL: compressedURL) { (exportSession) in
            guard let session = exportSession else {
                return
            }
            
            switch session.status {
                
            case .unknown:
                break
            case .waiting:
                break
            case .exporting:
                break
            case .completed:
                guard let compressedData = NSData(contentsOf: self.compressedURL) else {
                    return
                }
                print("File size after compression: \(Double(compressedData.length / 1048576)) mb")
            case .failed:
                break
            case .cancelled:
                break
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        // add custom components
        let cancelButton = UIButton(frame: CGRect(x: 10.0, y: 20.0, width: 30.0, height: 30.0))
        cancelButton.setImage(#imageLiteral(resourceName: "ic_cancel"), for: UIControlState())
        cancelButton.addTarget(self, action: #selector(cancel), for: .touchUpInside)
        view.addSubview(cancelButton)
        
        if _from != Constant.FROM_CHATTING {
            
            //create label
            let wellLabel = UILabel(frame: CGRect(x:0, y: 0, width : 300, height: 30))
            wellLabel.text = "Well Done!!"
            wellLabel.textColor = UIColor.white
            wellLabel.textAlignment = NSTextAlignment.center
            wellLabel.font = UIFont(name:"HelveticaNeue", size: 25)
            wellLabel.backgroundColor = UIColor.clear
            wellLabel.center = CGPoint(x: view.center.x, y: 115)
            view.addSubview(wellLabel)
            
            let dareLabel = UILabel(frame: CGRect(x:0, y: 0, width : 300, height: 30))
            dareLabel.text = "Dare Recorded"
            dareLabel.textColor = UIColor.white
            dareLabel.textAlignment = NSTextAlignment.center
            dareLabel.font = UIFont(name:"HelveticaNeue", size: 25)
            dareLabel.backgroundColor = UIColor.clear
            dareLabel.center = CGPoint(x: view.center.x, y: 145)
            view.addSubview(dareLabel)
            
            sendButton = UIButton(frame: CGRect(x: 0, y: 0, width: 150, height: 50.0))
            sendButton.addTarget(self, action: #selector(sendAction(_:)), for: .touchUpInside)
            sendButton.setTitle("Send privately", for: .normal)
            sendButton.setTitleColor(UIColor.white, for: .normal)
            sendButton.backgroundColor = Constant.mainColor
            sendButton.center = CGPoint(x: view.center.x, y: view.center.y + 200)
            sendButton.layer.cornerRadius = 5.0
            sendButton.layer.masksToBounds = true
            self.view.addSubview(sendButton)
         
        }
    }
    
    @objc fileprivate func sendAction(_ sender: Any) {
        
        showLoadingView()
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                multipartFormData.append("\(AppDelegate.getUser()._id)".data(using:String.Encoding.utf8)!, withName: "user_id")
                multipartFormData.append("\(self._target_id)".data(using:String.Encoding.utf8)!, withName: "sender_id")
                multipartFormData.append("\(2)".data(using:String.Encoding.utf8)!, withName: "type")
                multipartFormData.append(URL(fileURLWithPath: self.compressedURL.path), withName: "file")
                
        },
            to: Constant.REQ_SENDTOREADYPLAY,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print("send readyplay start response : ", response)
                        
                        if let result = response.result.value {
                            
                            self.hideLoadingView()
                            
                            let dict = JSON(result)
                            
                            let result_code = dict[Constant.RES_RESULTCODE].intValue
                            
                            if result_code == Constant.CODE_SUCCESS {
                                
                                self.showToast("Successfully sent")
                                
                                //TODO SUCCESS :
                                if let viewControllers = self.navigationController?.viewControllers {
                                   
                                    //let viewControllers = self.navigationController!.viewControllers
                                    
                                    if self._from == Constant.FROM_READYPLAYLIST || self._from == Constant.FROM_INVITES {
                                        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 4], animated: true)
                                    } else {
                                        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
                                    }
                                }
                            } else {
                                
                                self.hideLoadingView()
                                self.showAlertDialog(title: R.string.ERROR, message: R.string.UPLOAD_FAIL, positive: R.string.OK, negative: nil)
                            }   
                            
                        } else  {
                            
                            self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.hideLoadingView()
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    return
                }
        }
        )
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        player?.play()
    }
    
    func cancel() {

        self.navigationController?.popViewController(animated: true)
    }
    
    @objc fileprivate func playerItemDidReachEnd(_ notification: Notification) {
        if self.player != nil {
            self.player!.seek(to: kCMTimeZero)
            self.player!.play()
        }
    }
}


extension VideoViewController: AVCaptureFileOutputRecordingDelegate {
    
    func capture(_ captureOutput: AVCaptureFileOutput!, didFinishRecordingToOutputFileAt outputFileURL: URL!, fromConnections connections: [Any]!, error: Error!) {
        guard let data = NSData(contentsOf: outputFileURL as URL) else {
            return
        }
        
        print("File size before compression: \(Double(data.length / 1048576)) mb")
        let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".m4v")
        compressVideo(inputURL: outputFileURL as URL, outputURL: compressedURL) { (exportSession) in
            guard let session = exportSession else {
                return
            }
            
            switch session.status {
            case .unknown:
                break
            case .waiting:
                break
            case .exporting:
                break
            case .completed:
                guard let compressedData = NSData(contentsOf: compressedURL) else {
                    return
                }
                
                print("File size after compression: \(Double(compressedData.length / 1048576)) mb")
            case .failed:
                break
            case .cancelled:
                break
            }
        }
    }
    
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetLowQuality) else {
            handler(nil)
            
            return
        }
        
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileTypeQuickTimeMovie
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
}
