//
//  IntroViewController.swift
//  Krepta
//
//  Created by Beautistar on 6/7/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import SwiftyJSON
import SwiftyUserDefaults
import Alamofire
import EBForeNotification


class IntroViewController: BaseViewController {
    
    var dict : [String : AnyObject]!
    var _notifications = [String]()
    
    @IBOutlet weak var lblTerm: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        initView()
    }
    func initView() {
        
        //hide navigation bar
        self.navigationController?.setNavigationBarHidden(true, animated: false)

        //Term label setting
        lblTerm.text = "By signing up you agree to our Terms and Privacy Policy"
        let text = (lblTerm.text)!
        let underlineAttriString = NSMutableAttributedString(string: text)
        let range1 = (text as NSString).range(of: "Terms")
        underlineAttriString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range1)
        let range2 = (text as NSString).range(of: "Privacy Policy")
        underlineAttriString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range2)
        lblTerm.attributedText = underlineAttriString       
        
        if Defaults[.id] > 0 {            
            
            AppDelegate.getUser()._id  = Defaults[.id]
           
            if Defaults[.email] != nil { AppDelegate.getUser()._email = Defaults[.email]!}
            if Defaults[.username] != nil { AppDelegate.getUser()._username = Defaults[.username]!}
            if Defaults[.name] != nil { AppDelegate.getUser()._name = Defaults[.name]!}
            if Defaults[.gender] != nil { AppDelegate.getUser()._gender = Defaults[.gender]!}
            if Defaults[.status] != nil { AppDelegate.getUser()._status = Defaults[.status]!}
            if Defaults[.condition] != nil { AppDelegate.getUser()._condition = Defaults[.condition]!}
            if Defaults[.photo_urls] != nil { AppDelegate.getUser()._profileUrls = Defaults[.photo_urls]!}
            
        
            createMenuView()
        }
    }
    
    func createMenuView() {
        
        // create viewController code...
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "MainTabViewController") as! MainTabViewController
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        let rightViewController = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        nvc.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Space Age", size: 20)!, NSForegroundColorAttributeName: UIColor.white]
        
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        
        UINavigationBar.appearance().barTintColor = Constant.barTintColor
        
        UINavigationBar.appearance().layer.shadowColor = UIColor.black.cgColor
        UINavigationBar.appearance().layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        leftViewController.mainViewController = nvc
        
        let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: leftViewController, rightMenuViewController: rightViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = mainViewController as? SlideMenuControllerDelegate
        UIApplication.shared.keyWindow?.rootViewController = slideMenuController
        UIApplication.shared.keyWindow?.makeKeyAndVisible()
    }
    
    
    @IBAction func tapLabel(gesture: UITapGestureRecognizer) {
        
        let text = (lblTerm.text)!
        let termsRange = (text as NSString).range(of: "Terms")
        let privacyRange = (text as NSString).range(of: "Privacy Policy")
        
        if gesture.didTapAttributedTextInLabel(label:lblTerm, inRange: termsRange) {
            print("Tapped terms")
            
        } else if gesture.didTapAttributedTextInLabel(label: lblTerm, inRange: privacyRange) {
            print("Tapped privacy")
            gotoPrivacyVC()
        } else {
            print("Tapped none")
        }
    }
    
    func gotoPrivacyVC() {
        
        let privacyVC = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyViewController") as! PrivacyViewController
        self.navigationController?.pushViewController(privacyVC, animated: true)        
    }
    
    @IBAction func fbLoginAction(_ sender: Any) {        
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["public_profile", "email", "user_friends", "user_birthday", "user_photos"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData()
                        
                        fbLoginManager.logOut()
                    }
                }
            }
        }
    }
    
    func getFBUserData(){
    
        if((FBSDKAccessToken.current()) != nil) {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, first_name, picture.type(large), gender, email, birthday, albums.fields(name, photos.fields(source))"]).start(completionHandler: { (connection, result, error) -> Void in
                
                if (error == nil) {
                    
                    let jsonResponse = JSON(result!)
                    
                    print("jsonResponse", jsonResponse)
                    
                    let id = jsonResponse["id"].stringValue
                    //let profileURL = String(format: "https://graph.facebook.com/%@/picture?type=large", id)/// You can change profile image size : large, square, small, normal <- select one
                    
                    let email = jsonResponse["email"].string ?? String(format: "%@@facebook.com", id)
                    let gender = jsonResponse["gender"].string ?? "Anyone"
                    let name = jsonResponse["first_name"].string ?? "unknown"
                    let birthday = jsonResponse["birthday"].string ?? "01/01/1990"
                    
                    if let jsonAlbums = jsonResponse["albums"]["data"].array {
                        
                        FBAlbum.removeAll()
                        for jsonAlbum in jsonAlbums {
                            
                            let albumEntity = FBAlbumEntity()
                            
                            if let _id = jsonAlbum["id"].string {
                                albumEntity._id = _id
                            }
                            
                            if let _name = jsonAlbum["name"].string {
                                albumEntity._name = _name
                            }
                            
                            if let jsonPhotos = jsonAlbum["photos"]["data"].array {
                                
                                for photo in jsonPhotos {
                                    albumEntity._pictureUrls.append(photo["source"].stringValue)
                                }
                            }
                            
                            FBAlbum.append(albumEntity)
                            //
                        }
                    }
                    
                    /// save facebook album into userdefault as object array
                    let encodedData = NSKeyedArchiver.archivedData(withRootObject: FBAlbum)
                    UserDefaults.standard.set(encodedData, forKey: "fb_album")
                    UserDefaults.standard.synchronize()

                    let birthdayArr : [String] = birthday.components(separatedBy: "/")
                    
                    let myDOB = Calendar.current.date(from: DateComponents(year: Int(birthdayArr[2]), month: Int(birthdayArr[0]), day: Int(birthdayArr[1])))!
                    
                    let age = myDOB.age
                    
                    AppDelegate.getUser()._name = name
                    AppDelegate.getUser()._email = email
                    AppDelegate.getUser()._age = age
                    AppDelegate.getUser()._gender = gender
                    AppDelegate.getUser()._profileUrls[0] = FBAlbum[0]._pictureUrls[0]
                    
                    for fbAlbum in FBAlbum {
                        if fbAlbum._name == "Profile Pictures" {
                           AppDelegate.getUser()._profileUrls[0] = fbAlbum._pictureUrls[0]
                            break
                        }
                    }
                    
                    /// check user already exist
                    self.checkUserExist()

                } else {
                    // TODO :  Exeption  ****
                    print(error!)
                    
                }
            })
        } else {
            print("token is null")
        }
    }
    
    func checkUserExist() {
        
        showLoadingView()
        
        let token = Defaults[.token] ?? "unknown"
        
        let URL = Constant.REQ_CHECKEMAILEXIST + "\(AppDelegate.getUser()._email)/\(token)/"
        
        print(URL)
        
        Alamofire.request(URL, method:.get)
            .responseJSON { response in
                
                if response.result.isFailure {
                    
                    self.hideLoadingView()
                    self.showToast(R.string.CONNECT_FAIL)
                    return
                }
                
                if let result = response.result.value  {
                    
                    let dict = JSON(result)
                    
                    print(dict)
                    
                    let result_code = dict[Constant.RES_RESULTCODE].intValue
                    
                    if result_code == Constant.CODE_SUCCESS {
                        
                        self.hideLoadingView()
                        
                        let userInfo = dict[Constant.RES_USERINFO]
                        
                        AppDelegate.getUser()._id = userInfo["id"].intValue
                        
                        AppDelegate.getUser()._name = userInfo["name"].stringValue
                        AppDelegate.getUser()._username = userInfo["username"].stringValue
                        AppDelegate.getUser()._email = userInfo["email"].stringValue
                        AppDelegate.getUser()._age = userInfo["age"].intValue
                        AppDelegate.getUser()._gender = userInfo["gender"].stringValue
                        AppDelegate.getUser()._condition = userInfo["condition"].stringValue
                        AppDelegate.getUser()._status = userInfo["status"].stringValue
                        let photo_urls = userInfo["photo_url"].arrayValue
                        AppDelegate.getUser()._profileUrls.removeAll()
                        for p_url in photo_urls {
                            AppDelegate.getUser()._profileUrls.append(p_url.stringValue)
                        }
                        
                        //parse missed notification
                        
                        self._notifications.removeAll()
                        let notifications = dict["notification"].arrayValue
                        
                        for noti_one in notifications {
                            
                            self._notifications.insert(noti_one["content"].stringValue.decodeEmoji, at: 0)
                        }
                        self.sendNotification()
                        
                        Defaults[.id] = AppDelegate.getUser()._id
                        Defaults[.name] = AppDelegate.getUser()._name
                        Defaults[.username] = AppDelegate.getUser()._username
                        Defaults[.email] = AppDelegate.getUser()._email
                        Defaults[.age] = AppDelegate.getUser()._age
                        Defaults[.gender] = AppDelegate.getUser()._gender
                        Defaults[.condition] = AppDelegate.getUser()._condition
                        Defaults[.status] = AppDelegate.getUser()._status
                        Defaults[.photo_urls] = AppDelegate.getUser()._profileUrls
                        
                        /// for open explaination popviewcontroller when just login
                        _loggedIn = true
                        
                        self.createMenuView()
                    
                    } else if result_code == 201 {
                        self.hideLoadingView()
                        self.gotoProfilePhotoVC(profileUrl: AppDelegate.getUser()._profileUrls[0])
                    }
                }
        }
    }
    
    func gotoProfilePhotoVC(profileUrl:String) {
        
        let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfilePhotoViewController") as! ProfilePhotoViewController
        profileVC._imgUrl1 = profileUrl
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    var notiIndex = 0
    var notiTimer: Timer!
    
    func sendNotification() {
        
        notiTimer =  Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(IntroViewController.aTimer), userInfo: nil, repeats: true)
    }
    
    func aTimer() {
        
        if notiIndex < self._notifications.count {
            
            EBForeNotification.handleRemoteNotification(["aps":["alert":self._notifications[notiIndex]]], soundID: 1312)
            notiIndex += 1
            
        } else {
            
            notiTimer.invalidate()
        }
    }
}

extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        
        let textContainerOffset = CGPoint(x:(labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                          y:(labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let locationOfTouchInTextContainer = CGPoint(x:locationOfTouchInLabel.x - textContainerOffset.x, y:locationOfTouchInLabel.y - textContainerOffset.y);
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
}

extension Date {
    var age: Int {
        return Calendar.current.dateComponents([.year], from: self, to: Date()).year!
    }
}
