//
//  ProfilePhotoViewController.swift
//  Krepta
//
//  Created by Beautistar on 17/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import MobileCoreServices
import Alamofire
import SwiftyJSON
import SDWebImage
import SwiftyUserDefaults

class ProfilePhotoViewController: BaseViewController, UIImagePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var image1: UIImageView!
    @IBOutlet weak var image2: UIImageView!
    @IBOutlet weak var image3: UIImageView!
    @IBOutlet weak var image4: UIImageView!
    
    @IBOutlet weak var doneButton: UIButton!
    
    let _picker: UIImagePickerController = UIImagePickerController()
    var _imgUrl1 : String?
    var _imgUrl2 : String?
    var _imgUrl3 : String?
    var _imgUrl4 : String?
    var _selectedImage : Int = 0
    var _user_status = ""
    var _from = 0
    var _selectedPhotoURLs = [String:String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self._picker.delegate = self
        _picker.allowsEditing = true
        
        initView()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Space Age", size: 20)!, NSForegroundColorAttributeName: UIColor.white]        
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        if AppDelegate.getUser()._profileUrls[1].length != 0 {
            doneButton.isHidden = false
        }
        
        //check if images was taken from facebook album
        if _selectedAlbumPhotoURL.length > 0 {
            
            switch _selectedImage {
                
            case 0:
                image1.sd_setImage(with: URL(string : _selectedAlbumPhotoURL), placeholderImage: #imageLiteral(resourceName: "img_user"))
               AppDelegate.getUser()._profileUrls[0] = _selectedAlbumPhotoURL
                _selectedAlbumPhotoURL = ""
                break
            case 1:
                image2.sd_setImage(with: URL(string : _selectedAlbumPhotoURL), placeholderImage: #imageLiteral(resourceName: "img_user"))
                AppDelegate.getUser()._profileUrls[1] = _selectedAlbumPhotoURL
                _selectedAlbumPhotoURL = ""
                break
                
            case 2:
                image3.sd_setImage(with: URL(string : _selectedAlbumPhotoURL), placeholderImage: #imageLiteral(resourceName: "img_user"))
                AppDelegate.getUser()._profileUrls[2] = _selectedAlbumPhotoURL
                _selectedAlbumPhotoURL = ""
                break
            default:
                image4.sd_setImage(with: URL(string : _selectedAlbumPhotoURL), placeholderImage: #imageLiteral(resourceName: "img_user"))
                AppDelegate.getUser()._profileUrls[3] = _selectedAlbumPhotoURL
                _selectedAlbumPhotoURL = ""
                break
            }
        }
    }
    
    func initView() {        
        
        if _from == Constant.FROM_PROFILE {
            doneButton.isHidden = false
            
            image1.sd_setImage(with: URL(string:AppDelegate.getUser()._profileUrls[0]), placeholderImage:#imageLiteral(resourceName: "img_user"))
            
            for index in 0..<AppDelegate.getUser()._profileUrls.count {
                
                switch index {
                case 1:
                    image2.sd_setImage(with: URL(string: AppDelegate.getUser()._profileUrls[1]), placeholderImage: #imageLiteral(resourceName: "img_user"))
                    break
                case 2:
                    image3.sd_setImage(with: URL(string: AppDelegate.getUser()._profileUrls[2]), placeholderImage: #imageLiteral(resourceName: "img_user"))
                    break
                default:
                    image4.sd_setImage(with: URL(string: AppDelegate.getUser()._profileUrls[3]), placeholderImage: #imageLiteral(resourceName: "img_user"))
                    break
                }
            }
        } else {
            image1.sd_setImage(with: URL(string:_imgUrl1!), placeholderImage:#imageLiteral(resourceName: "img_user"))
            AppDelegate.getUser()._profileUrls[0] = _imgUrl1!
            doneButton.isHidden = true
        }        
    }
    
    @IBAction func attachAction(_ sender:UIButton) {
        
        _selectedImage = sender.tag
        
        doAttach()        
        
    }
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Attact Image/Video
    
    func doAttach() {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
        {
            let photoSourceAlert: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let albumAction: UIAlertAction = UIAlertAction(title: R.string.CAMERA_ROLL, style: UIAlertActionStyle.default, handler: {
                (photoSourceAlert) -> Void in
                self._picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                self.present(self._picker, animated: true, completion: nil)
            })
            
            let fbAlbumAction: UIAlertAction = UIAlertAction(title: R.string.FACEBOOK, style: UIAlertActionStyle.default, handler: {
                (photoSourceAlert) -> Void in
                self.gotoFacebookAlbum()
                //self.present(self._picker, animated: true, completion: nil)
            })
            
            photoSourceAlert.addAction(albumAction)
            photoSourceAlert.addAction(fbAlbumAction)
            photoSourceAlert.addAction(UIAlertAction(title: R.string.CANCEL, style: UIAlertActionStyle.cancel, handler: nil))
            
            self.present(photoSourceAlert, animated: true, completion: nil);
        }

    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            
            //self._imgUrl.image = pickedImage
            
            doneButton.isHidden = false
            
            print(_imgUrl1 ?? "NONE")
            
            switch _selectedImage {
            case 0:
                image1.image = nil
                _imgUrl1 = saveToFile(image: pickedImage, filePath: Constant.SAVE_ROOT_PATH, fileName: "image1.png")
                //upload to server and get URL
                self.uploadPhoto(_index: 0, _imgUrl: _imgUrl1!)
                break
            case 1:
                image2.image = nil
                _imgUrl2 = saveToFile(image: pickedImage, filePath: Constant.SAVE_ROOT_PATH, fileName: "image2.png")
                self.uploadPhoto(_index: 1, _imgUrl: _imgUrl2!)
                break
                
            case 2:
                image3.image = nil
                _imgUrl3 = saveToFile(image: pickedImage, filePath: Constant.SAVE_ROOT_PATH, fileName: "image3.png")
                self.uploadPhoto(_index: 2, _imgUrl: _imgUrl3!)
                break
            default:
                image4.image = nil
                _imgUrl4 = saveToFile(image: pickedImage, filePath: Constant.SAVE_ROOT_PATH, fileName: "image4.png")
                self.uploadPhoto(_index: 3, _imgUrl: _imgUrl4!)
                break
            }
        }
        
        if let videoURL = info[UIImagePickerControllerMediaURL] as? NSURL {
            print(videoURL)
        }
        
        dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }
    
    func uploadPhoto(_index: Int, _imgUrl: String) {
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                multipartFormData.append(URL(fileURLWithPath: _imgUrl), withName: "file")                
                
        },
            to: Constant.REQ_UPLOADIMAGE,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print("upload image response : ", response)
                        
                        if let result = response.result.value {
                            
                            let dict = JSON(result)
                            
                            let result_code = dict[Constant.RES_RESULTCODE].intValue
                            
                            if result_code == Constant.CODE_SUCCESS {
                                
                                AppDelegate.getUser()._profileUrls[_index] = dict[Constant.RES_PHOTOURL].stringValue
                                
                                switch _index {
                                    
                                case 0: self.image1.sd_setImage(with: URL(string:dict[Constant.RES_PHOTOURL].stringValue), placeholderImage: #imageLiteral(resourceName: "img_user"))
                                    break
                                case 1: self.image2.sd_setImage(with: URL(string:dict[Constant.RES_PHOTOURL].stringValue), placeholderImage: #imageLiteral(resourceName: "img_user"))
                                    break
                                case 2: self.image3.sd_setImage(with: URL(string:dict[Constant.RES_PHOTOURL].stringValue), placeholderImage: #imageLiteral(resourceName: "img_user"))
                                    break
                                default :
                                    self.image4.sd_setImage(with: URL(string:dict[Constant.RES_PHOTOURL].stringValue), placeholderImage: #imageLiteral(resourceName: "img_user"))
                                    break
                                    
                                }
                            }
                            
                        } else  {
                            
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.hideLoadingView()
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    return
                    
                }
        }
        )
    }
    
    func gotoFacebookAlbum() {
        
        let albumListVC = self.storyboard?.instantiateViewController(withIdentifier: "AlbumListViewController") as! AlbumListViewController
        self.navigationController?.pushViewController(albumListVC, animated: true)
    }
    
    @IBAction func doneAction(_ sender: Any) {
        
        if _from == Constant.FROM_PROFILE {
            //update profile API
            updateProfile()
        } else {
            //goto username vc
            gotoUserNameVC()
        }
    }
    
    func gotoUserNameVC() {
        
        let usernameVC = self.storyboard?.instantiateViewController(withIdentifier: "NameViewController") as! NameViewController
        self.navigationController?.pushViewController(usernameVC, animated: true)
    }
    
    func updateProfile() {
        
        showLoadingView()
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                multipartFormData.append("\(AppDelegate.getUser()._id)".data(using:String.Encoding.utf8)!, withName: "id")
                
                for index in 0 ..< AppDelegate.getUser()._profileUrls.count {
                    
                    multipartFormData.append("\(AppDelegate.getUser()._profileUrls[index])".data(using:String.Encoding.utf8)!, withName: "photo_urls[\(index)]")
                }
        },
            to: Constant.REQ_UPDATEPROFILE,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print("update profile response : ", response)
                        
                        if let result = response.result.value {
                            
                            self.hideLoadingView()
                            
                            let dict = JSON(result)
                            
                            let result_code = dict[Constant.RES_RESULTCODE].intValue
                            
                            if result_code == Constant.CODE_SUCCESS {                                

                                Defaults[.photo_urls] = AppDelegate.getUser()._profileUrls
                                
                                self.navigationController?.popViewController(animated: true)
                            }
                            
                        } else  {
                            
                            self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.hideLoadingView()
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    return
                    
                }
        }
        )

    }
}
