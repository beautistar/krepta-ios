//
//  NameViewController.swift
//  Krepta
//
//  Created by Beautistar on 6/9/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SwiftyUserDefaults

class NameViewController: BaseViewController, UITextFieldDelegate {

    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var tfUserName: UITextField!
    @IBOutlet weak var imvCheckStatus: UIImageView!
    
    var isAvailable:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        imvCheckStatus.image = UIImage(named: "")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func isValid() -> Bool {
        
        if !isAvailable {
            
            showAlertDialog(title: nil, message: R.string.INPUT_USERNAME, positive: R.string.OK, negative: nil)
            return false
        }
        
        let currentUser = AppDelegate.getUser()
        
        if currentUser._latitude == 0 || currentUser._longitude == 0 {

            self.showToast("search location")
            return false
        }
        
        return true
    }
    
    
    
    @IBAction func confirmAction(_ sender: Any) {
        
        if isValid() {
            
            doRegister()
        }
    }
    
    func checkUserName() {
        
        self.showLoadingView()
        
        let username = tfUserName.text!.encodeString()!
        
        let URL = Constant.REQ_CHECKUSERNAME + "\(username)/"
        
        print(URL)
        
        Alamofire.request(URL, method:.get)
            .responseJSON { response in
                
                if response.result.isFailure {
                    
                    self.hideLoadingView()
                    self.showToast(R.string.CONNECT_FAIL)
                    return
                }
                
                if let result = response.result.value  {
                    
                    self.hideLoadingView()
                    
                    let dict = JSON(result)
                    
                    let result_code = dict[Constant.RES_RESULTCODE].intValue
                    
                    if result_code == Constant.CODE_SUCCESS {                        

                        Defaults[.username] = self.tfUserName.text!
                        self.imvCheckStatus.image = #imageLiteral(resourceName: "ic_available")
                        self.isAvailable = true
                        
                    } else if result_code == 201 {
                        
                        self.imvCheckStatus.image = #imageLiteral(resourceName: "ic_unavailable")
                        self.isAvailable = false
                    }
                }
        }
    }
    
    func doRegister() {
        
        self.showLoadingView()
        
        let username = tfUserName.text!.encodeString()!        
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                multipartFormData.append(username.data(using:String.Encoding.utf8)!, withName: "username")
                multipartFormData.append("\(AppDelegate.getUser()._email)".data(using:String.Encoding.utf8)!, withName: "email")
                multipartFormData.append("\(AppDelegate.getUser()._name)".data(using:String.Encoding.utf8)!, withName: "name")
                multipartFormData.append("\(AppDelegate.getUser()._gender)".data(using:String.Encoding.utf8)!, withName: "gender")
                multipartFormData.append("\(AppDelegate.getUser()._age)".data(using:String.Encoding.utf8)!, withName: "age")
                multipartFormData.append("\(AppDelegate.getUser()._latitude)".data(using:String.Encoding.utf8)!, withName: "latitude")
                multipartFormData.append("\(AppDelegate.getUser()._longitude)".data(using:String.Encoding.utf8)!, withName: "longitude")
                let token = Defaults[.token] ?? "unknown"
                multipartFormData.append(token.data(using:String.Encoding.utf8)!, withName: "token")
                
                for index in 0 ..< AppDelegate.getUser()._profileUrls.count {
                    
                    multipartFormData.append("\(AppDelegate.getUser()._profileUrls[index])".data(using:String.Encoding.utf8)!, withName: "photo_urls[\(index)]")
                }
        },
            to: Constant.REQ_REGISTER,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print("register response : ", response)
                        
                        if let result = response.result.value {
                            
                            self.hideLoadingView()
                            
                            let dict = JSON(result)
                            
                            let result_code = dict[Constant.RES_RESULTCODE].intValue
                            
                            if result_code == Constant.CODE_SUCCESS {
                                
                                AppDelegate.getUser()._id = dict[Constant.RES_ID].intValue
                                AppDelegate.getUser()._username = self.tfUserName.text!
                                
                                Defaults[.id] = AppDelegate.getUser()._id
                                Defaults[.email] = AppDelegate.getUser()._email
                                Defaults[.name] = AppDelegate.getUser()._name
                                Defaults[.age] = AppDelegate.getUser()._age
                                Defaults[.gender] = AppDelegate.getUser()._gender
                                Defaults[.username] = self.tfUserName.text!
                                Defaults[.isRegister] = true
                                Defaults[.latitude] = AppDelegate.getUser()._latitude
                                Defaults[.longitude] = AppDelegate.getUser()._longitude
                                Defaults[.photo_urls] = AppDelegate.getUser()._profileUrls
                                
                                _loggedIn = true
                                self.createMenuView()
                            }
                        
                        } else  {
                            
                            self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.hideLoadingView()
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)                    
                    return
                    
                }
        }
        )
    }
    
    func createMenuView() {
        
        // create viewController code...
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "MainTabViewController") as! MainTabViewController
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        let rightViewController = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        nvc.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Space Age", size: 20)!, NSForegroundColorAttributeName: UIColor.white]
        
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        
        UINavigationBar.appearance().barTintColor = Constant.barTintColor
        
        UINavigationBar.appearance().layer.shadowColor = UIColor.black.cgColor
        UINavigationBar.appearance().layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        leftViewController.mainViewController = nvc
        
        let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: leftViewController, rightMenuViewController: rightViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = mainViewController as? SlideMenuControllerDelegate
        UIApplication.shared.keyWindow?.rootViewController = slideMenuController
        UIApplication.shared.keyWindow?.makeKeyAndVisible()
    }
    
    //MAEK: - TextField return key delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        isAvailable = false
        self.imvCheckStatus.image = UIImage(named: "")
        return true
    }
    
    @IBAction func onEdit(_ sender: Any) {
        
        isAvailable = false
        if !(tfUserName.text?.isEmpty)! && (tfUserName.text?.length)! > 2 {
            checkUserName()
        }
    }
}
