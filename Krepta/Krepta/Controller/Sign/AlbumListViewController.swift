//
//  AlbumListViewController.swift
//  Krepta
//
//  Created by Beautistar on 8/14/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class AlbumListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblAlbumList: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        FBAlbum.removeAll()
        // retrieving a value for a key
        if let data = UserDefaults.standard.data(forKey: "fb_album"),
            let fbalbum = NSKeyedUnarchiver.unarchiveObject(with: data) as? [FBAlbumEntity] {
            FBAlbum = fbalbum
            FBAlbum.forEach({print( $0._id, $0._name, $0._pictureUrls)})  
        } else {
            print("There is an issue")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Space Age", size: 20)!, NSForegroundColorAttributeName: UIColor.white]
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }

    // MARK: - TableView delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return FBAlbum.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AlbumListCell") as! AlbumListCell
        
        var first_photoURL = ""
        let album_one = FBAlbum[indexPath.row]
        
        if album_one._pictureUrls.count > 0 {
            first_photoURL = album_one._pictureUrls[0]
        }

        cell.imvPhoto.sd_setImage(with: URL(string: first_photoURL), placeholderImage: #imageLiteral(resourceName: "img_user"))
        cell.lblAlbumName.text = FBAlbum[indexPath.row]._name
        if FBAlbum[indexPath.row]._pictureUrls.count == 1 {
            cell.lblPhotoCounts.text = String(format : "%d photo", FBAlbum[indexPath.row]._pictureUrls.count)
        } else {
            cell.lblPhotoCounts.text = String(format : "%d photos", FBAlbum[indexPath.row]._pictureUrls.count)
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {        
        
        let albumPictureVC = self.storyboard?.instantiateViewController(withIdentifier: "AlbumPictureViewController") as! AlbumPictureViewController
        albumPictureVC._selectedAlbum = FBAlbum[indexPath.row]
        self.navigationController?.pushViewController(albumPictureVC, animated: true)
    }
}
